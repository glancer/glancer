#ifndef GLANCER_MESH_H_
#define GLANCER_MESH_H_

#include <unordered_map>
#include <vector>

#include "Point3D.h"
#include "MeshFace.h"
#include "MeshVertex.h"

namespace glancer {
    class Polygon;
    class SurfacePlane;

class Mesh {
  public:
    Mesh();
    ~Mesh();
    /**
     * This function will return index of the face inserted
     * If there were the same points for at least 1 edge, it will return 0
     */
    size_t AddFace(Point3D &vrtx0, Point3D &vrtx1, Point3D &vrtx2);
    /**
     * Returns number of vertices in the mesh
     */
    size_t getNofVertices() const;
    /**
     * Returns number of faces
     */
    size_t getNofFaces() const;
    /**
     * The bounding box min point
     */
    Point3D& getMin();
    /**
     * The bounding box max point
     */
    Point3D& getMax();
    /**
     * Slices this mesh with Provided Plane surface and creates polygon
     * The resulting polygon is not checked for all the errors it might have
     */
    Polygon SliceWithPlane(const SurfacePlane &surface);

  private:
    /**
     * This function will return the index of the vertex if the point is already in vertices map
     * If the point is not in array it will pick up next available index, add vertex and return the resulting
     * index
     */
    size_t AddVertex(Point3D &p);
    

  private:
    std::unordered_map <size_t, MeshFace> faces;
    size_t nav_face_idx; // Stores next available face index, this will used if avail_face_idx is empty when the face is added.
    std::vector <size_t> avail_face_idx; // List of deleted face indexes, that are available.

    // This is a crude way of storing both reverse and forward maps for vertices
    // Though it doubles the amount of memory, but allows fast forward and reverse access.
    std::unordered_map <size_t, MeshVertex> vertices;
    /**
     * Warning vertices_reverse is used only in AddVertex function to check if the given vertex was added
     * or not. So it should never be used anywhere outside... There will be a freezing of the mesh
     * implemented later on which will clear and regenerate this array if no point additions would be made to the mesh.
     */
    std::unordered_map <Point3D, size_t, Point3D_hash>    vertices_reverse;
    size_t nav_vert_idx;
    std::vector <size_t> avail_vert_idx;
    /**
     * Bounding box recalculated every time the vertex is added
     */
    Point3D min;
    Point3D max;
};
} // namespace glancer
#endif // GLANCER_MESH_H_
