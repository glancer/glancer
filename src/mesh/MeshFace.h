#ifndef GLANCER_MESHFACE_H_
#define GLANCER_MESHFACE_H_

#include <cstddef>  // for std::size_t

namespace glancer {

class MeshFace {
    friend class Mesh;
  public:
    MeshFace(std::size_t vrtx1, std::size_t vrtx2, std::size_t vrtx3);
    ~MeshFace();
  private:
    std::size_t vertex_index[3];
};
} // namespace glancer
#endif // GLANCER_MESHFACE_H_
