#include <stdio.h>  // For file operations

#include "MeshFileLoader.h"

namespace glancer {
//-------------------------------------------------------------------//
MeshFileLoader::MeshFileLoader(unsigned long mm_to_int) :
        conversion_factor(mm_to_int) {
}
//-------------------------------------------------------------------//
MeshFileLoader::~MeshFileLoader(){
}
//-------------------------------------------------------------------//
bool MeshFileLoader::LoadBinarySTL(std::string filename, Mesh& to_mesh) {
    FILE* f = fopen(filename.c_str(), "rb");
    if ( f == NULL ) {
        return false;
    }
    fseek(f, 0L, SEEK_END);
    long file_size = ftell(f);
    // Header is 80 bytes, each face is 50bytes.
    if (file_size < 80) {
        // No reason for continuing, because we don't have even a header
        fclose(f);
        return false;
    }
    rewind(f); // back to the beginning of the file
    size_t face_count = (file_size - 80 - sizeof(uint32_t)) / 50;
    if (face_count < 1) {
        // No reason for continuing because we don't have any faces
        fclose(f);
        return false;
    }
    // We're just ignoring the header
    fseek(f, 80, SEEK_CUR);
    uint32_t reported_face_count;
    if (fread(&reported_face_count, sizeof(uint32_t), 1, f) != 1) {
        fclose(f);
        return false;
    }
    // Check if the file is corrupted
    if (reported_face_count != face_count) {
        fclose(f);
        return false;
    }
    // OK, read the faces in sequential order and add them to mesh
    // right now we will not test if addFace function was successfull or not
    char buffer[50];
    // clear mesh???
    for (size_t ii=0; ii < face_count; ++ii) {
        if (fread(buffer, 50, 1, f) != 1) {
            fclose(f);
            return false;
        }
        float *v= ((float*)buffer)+3;
        Point3D v0 = Point3D( (long) (v[0]*conversion_factor), (long) (v[1]*conversion_factor), (long) (v[2]*conversion_factor) );
        Point3D v1 = Point3D( (long) (v[3]*conversion_factor), (long) (v[4]*conversion_factor), (long) (v[5]*conversion_factor) );
        Point3D v2 = Point3D( (long) (v[6]*conversion_factor), (long) (v[7]*conversion_factor), (long) (v[8]*conversion_factor) );
        to_mesh.AddFace(v0, v1, v2);
    }
    fclose(f);
    return true;
}
//-------------------------------------------------------------------//
} // namespace glancer
