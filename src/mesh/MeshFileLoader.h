#ifndef GLANCER_MESHFILELOADER_H_
#define GLANCER_MESHFILELOADER_H_
#include <string>

#include "Mesh.h"

namespace glancer {

class MeshFileLoader {
  public:
    MeshFileLoader(unsigned long mm_to_int);
    ~MeshFileLoader();

    bool LoadBinarySTL(std::string filename, Mesh& to_mesh);

  private:
    unsigned long conversion_factor; //< This is mm to Point3D multiplication factor
};
} // namespace glancer
#endif // GLANCER_MESHFILELOADER_H_
