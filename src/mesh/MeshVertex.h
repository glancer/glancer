#ifndef GLANCER_MESHVERTEX_H_
#define GLANCER_MESHVERTEX_H_

#include "Point3D.h"

#include <cstddef> // for size_t

namespace glancer {
/**
 * This class is essentially Point3D
 * But with additional connectivity info for Mesh
 * (like indexes of connected faces etc.)
 * If needed this info will be added here later
 */
class MeshVertex {
    friend class Mesh;
  public:
    MeshVertex(const Point3D& _p);
    ~MeshVertex();

  public:
    /**
     * Apparently unordered_map requires public default constructor
     */
    MeshVertex();

  public:
    Point3D p;
};
/**
 * This is used as a hash function in reverse vertex maps
 */
struct Point3D_hash {
    size_t operator() (const Point3D &p) const {
        return p.X() ^ (p.Y() << 10) ^ (p.Z() << 20);
    }
};
} // namespace glancer
#endif // GLANCER_MESHVERTEX_H_
