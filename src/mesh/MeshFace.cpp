#include "MeshFace.h"
namespace glancer {
//-------------------------------------------------------------------//
MeshFace::MeshFace(std::size_t vrtx1, std::size_t vrtx2, std::size_t vrtx3) :
        vertex_index {vrtx1,vrtx2,vrtx3} {
}
//-------------------------------------------------------------------//
MeshFace::~MeshFace(){
}
//-------------------------------------------------------------------//
} // namespace glancer
