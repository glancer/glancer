#include "Mesh.h"
#include "Polygon.h"        // For slicing
#include "SurfacePlane.h"   // For slicing
#include "Vector3D.h"       // For slicing

#include <algorithm>

#include <iostream> // for debugging, will need to be removed

namespace glancer {
//-------------------------------------------------------------------//
Mesh::Mesh() :
        nav_face_idx(1), nav_vert_idx(1),
        // Indexing is starting from 1, reserving 0 to no-vertex/face
        min(0,0,0), max(0,0,0) {
}
//-------------------------------------------------------------------//
Mesh::~Mesh(){
  faces.clear();
  avail_face_idx.clear();
  vertices.clear();
  vertices_reverse.clear();
  avail_vert_idx.clear();
}
//-------------------------------------------------------------------//
size_t Mesh::AddFace(Point3D &vrtx0, Point3D &vrtx1, Point3D &vrtx2){
    size_t vrtx_idx0 = AddVertex(vrtx0);
    size_t vrtx_idx1 = AddVertex(vrtx1);
    size_t vrtx_idx2 = AddVertex(vrtx2);

    if ( (vrtx_idx0 == vrtx_idx1 ) || (vrtx_idx0 == vrtx_idx2 ) || (vrtx_idx1 == vrtx_idx2 ) ) {
        /* 
        std::cout << "Same vertex face [" << vrtx_idx0 << "," << vrtx_idx1 << "," << vrtx_idx2 << "]" 
            << "V0=[" << vrtx0.X() << ";" << vrtx0.Y() << ";" << vrtx0.Z() << "]"
            << "V1=[" << vrtx1.X() << ";" << vrtx1.Y() << ";" << vrtx1.Z() << "]"
            << "V2=[" << vrtx2.X() << ";" << vrtx2.Y() << ";" << vrtx2.Z() << "]"
            << std::endl; 
        */
        return 0; // NoFace
    }
    size_t result;
    if (avail_face_idx.size() > 0 ) {
        // Let's try to grab index from avail_vert_idx first
        result = avail_face_idx[0];
        avail_face_idx.erase(avail_face_idx.begin());
    } else {
        result = nav_face_idx;
        nav_face_idx++;
    }
    faces.emplace(result, MeshFace(vrtx_idx0, vrtx_idx1, vrtx_idx2) );
    return result;
}
//-------------------------------------------------------------------//
size_t Mesh::getNofVertices() const{
    return vertices.size();
}
//-------------------------------------------------------------------//
size_t Mesh::getNofFaces() const{
    return faces.size();
}
//-------------------------------------------------------------------//
Point3D& Mesh::getMin() {
    return min;
}
//-------------------------------------------------------------------//
Point3D& Mesh::getMax() {
    return max;
}
//-------------------------------------------------------------------//
Polygon Mesh::SliceWithPlane(const SurfacePlane &surface) {
    Polygon result;

    // Let's go through all the faces. In principle we can
    // first convert all the mesh points to surface coordinates first, but
    // this would create a problem for multi-threaded access so 
    // I've decided to better have CPU times x3 increase.
    for (auto it=faces.begin(); it != faces.end(); ++it) {
        std::vector <int> above_idx;
        std::vector <int> below_idx;
        const int next_idx[3] = {1,2,0};
        const int prev_idx[3] = {2,0,1};
        // Get all 3 points in surface coordinates first
        Point3D point[3];
        point[0] = surface.ToSurface(vertices[it->second.vertex_index[0]].p);
        point[1] = surface.ToSurface(vertices[it->second.vertex_index[1]].p);
        point[2] = surface.ToSurface(vertices[it->second.vertex_index[2]].p);
        for (int ii = 0; ii < 3; ii++){
            if ( point[ii].Z() > 0 ) {
                above_idx.push_back(ii);
                continue;
            }
            if ( point[ii].Z() < 0 ) {
                below_idx.push_back(ii);
            }
        }
        // Let's filter out all 3 above or all 3 below case first
        if ((above_idx.size()==3) || (below_idx.size()==3) ) {
            continue;
        }
        Point2D from_point(0,0);
        Point2D to_point(0,0);
        // We are actually interested in 4 cases:
        // 1 above, 0 below <-- /\
        // 1 above, 1 below <-- <| or |>
        // 2 above, 1 below <-- \/
        // 1 above, 2 below <-- /|
        // We want to treat them differently...
        size_t segment_index = 0;
        if ( (above_idx.size()==1) && (below_idx.size()==0) ) {
            // std::cout << "Slice debug 1 above, 0 below ";
            from_point = point[ next_idx[above_idx[0]] ];
            to_point   = point[ prev_idx[above_idx[0]] ];
        } else if ( (above_idx.size()==1) && (below_idx.size()==1) ) {
            // std::cout << "Slice debug 2 above, 1 below ";
            Vector3D vector_offset(point[above_idx[0]], point[below_idx[0]] );
            long z_multiply = point[above_idx[0]].Z();
            long z_divide = z_multiply - point[below_idx[0]].Z();
            vector_offset *= z_multiply;
            vector_offset /= z_divide;
            // If the next index for above is below, then we're pushing from point_offset
            if (next_idx[above_idx[0]] == below_idx[0]) {
                from_point = point[above_idx[0]] + vector_offset;
                to_point = point[ prev_idx[above_idx[0]] ];
            } else {
                from_point = point[ next_idx[above_idx[0]] ];
                to_point = point[above_idx[0]] + vector_offset;
            }
        } else if ( (above_idx.size()==2) && (below_idx.size()==1) ) {
            // std::cout << "Slice debug 2 above, 1 below ";
            Vector3D from_vector_offset(point[ prev_idx[below_idx[0]] ], point[below_idx[0]] );
            long from_z_multiply = point[ prev_idx[below_idx[0]] ].Z();
            long from_z_divide = from_z_multiply - point[below_idx[0]].Z();
            from_vector_offset *= from_z_multiply; 
            from_vector_offset /= from_z_divide;
            from_point = point[ prev_idx[below_idx[0]] ] + from_vector_offset;
            Vector3D to_vector_offset(point[ next_idx[below_idx[0]] ], point[below_idx[0]] );
            long to_z_multiply = point[ next_idx[below_idx[0]] ].Z();
            long to_z_divide = to_z_multiply - point[below_idx[0]].Z();
            to_vector_offset *= to_z_multiply; to_vector_offset /= to_z_divide;
            to_point = point[ next_idx[below_idx[0]] ] + to_vector_offset;
        } else if ( (above_idx.size()==1) && (below_idx.size()==2) ) {
            // std::cout << "Slice debug 1 above, 2 below ";
            Vector3D from_vector_offset(point[above_idx[0]], point[ next_idx[above_idx[0]] ] );
            long from_z_multiply = point[above_idx[0]].Z();
            long from_z_divide = from_z_multiply - point[ next_idx[above_idx[0]] ].Z();
            from_vector_offset *= from_z_multiply; 
            from_vector_offset /= from_z_divide;
            from_point = point[above_idx[0]] + from_vector_offset;
            Vector3D to_vector_offset(point[above_idx[0]], point[ prev_idx[above_idx[0]] ] );
            long to_z_multiply = point[above_idx[0]].Z();
            long to_z_divide = to_z_multiply - point[ prev_idx[above_idx[0]] ].Z();
            to_vector_offset *= to_z_multiply; to_vector_offset /= to_z_divide;
            to_point = point[above_idx[0]] + to_vector_offset;
        } else {
            // std::cout << "other case below:" << std::endl;
            continue;
        }
        segment_index = result.AddSegment(from_point, to_point);
        /* 
        std::cout << " Segment index " << segment_index << " from [" 
            << from_point.X() << "," << from_point.Y() << "] to ["
            << to_point.X() << "," << to_point.Y() << "] resulted from "
            << "V0=[" << point[0].X() << ";" << point[0].Y() << ";" << point[0].Z() << "]"
            << "V1=[" << point[1].X() << ";" << point[1].Y() << ";" << point[1].Z() << "]"
            << "V2=[" << point[2].X() << ";" << point[2].Y() << ";" << point[2].Z() << "]"
            << std::endl;
        */
        
    }
    return result;
}
//-------------------------------------------------------------------//
size_t Mesh::AddVertex(Point3D &p) {
    // Let's see if the vertex is already there
    auto it = vertices_reverse.find(p);
    if (it != vertices_reverse.end()) {
        // We found the vertex point
        return it->second;
    }
    // We'll need to generate new index for this vertex
    size_t result;
    if (avail_vert_idx.size() > 0 ) {
        // Let's try to grab index from avail_vert_idx first
        result = avail_vert_idx[0];
        avail_vert_idx.erase(avail_vert_idx.begin());
    } else {
        result = nav_vert_idx;
        nav_vert_idx++;
    }
    MeshVertex vertex(p);
    vertices.emplace(result, vertex);
    vertices_reverse.emplace(p, result);
    // Recalculate the bounding box
    if (vertices.size() == 1) {
        // This was the first point added
        min = max = p;
    } else {
        if ( p.x > max.x ) max.x = p.x;
        if ( p.y > max.y ) max.y = p.y;
        if ( p.z > max.z ) max.z = p.z;
        if ( p.x < min.x ) min.x = p.x;
        if ( p.y < min.y ) min.y = p.y;
        if ( p.z < min.z ) min.z = p.z;
    }
    return result;
}
//-------------------------------------------------------------------//
} // namespace glancer
