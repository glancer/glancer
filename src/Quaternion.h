#ifndef GLANCER_QUATERNION_H_
#define GLANCER_QUATERNION_H_

#include "FVector3D.h"

namespace glancer {
    // Forward declarations
    class Matrix3D;
/*
 * After some thought I've decided to use normalized quaternions
 */
class Quaternion {
  public:
    /**
     * Empty constructor will initialize as ( {0,0,0} 1.0 ) quaternion
     */
    Quaternion();
    Quaternion(FVector3D& _r, float _w);
    ~Quaternion();
    /**
     * Read access to private class
     */
    FVector3D R() const;
    float W() const;
    /**
     * Sets quaternion from a rotation matrix
     */
    void FromMatrix(const Matrix3D& rot_matrix);
    /**
     * Sets rotation matrix from a quaternion
     */
    void ToMatrix(Matrix3D& rot_matrix) const;
    /**
     * Returns a vector rotated by quaternion
     * This operation is 2x slower comparing to matrices
     */
    FVector3D operator* (const FVector3D& orig_vec) const;
    /**
     * Returns inverse quaternion
     */
    Quaternion inverse() const;
    /**
     * Normalize the quaternion
     */
    void Normalize();

  private:
    FVector3D r;
    float w;
};
} // namespace glancer
#endif // GLANCER_QUATERNION_H_
