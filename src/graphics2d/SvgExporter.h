#ifndef GLANCER_SVGEXPORTER_H_
#define GLANCER_SVGEXPORTER_H_
#include <stdio.h>  // For file operations

#include "Point2D.h"
#include "PolyLine2D.h"

namespace glancer {
class PolygonSkeletonTree;
/**
 * Class for writing 2D objects to SVG file.
 */
class SvgExporter {
  public:
    class Color{
      public:
        Color(unsigned char _r, unsigned char _g, unsigned char _b);
        unsigned char r, g, b;
    };
  public:
    SvgExporter(Point2D& min, Point2D& max, float scale=1);
    ~SvgExporter();
    /**
     * Opens file for writing(truncated) and writes header to it
     * returns true if the file opens fine, returns false otherwise
     */
    bool Open(const char* filename);
    /**
     * Writes the closing section to the file (if it's open) and closes it
     */
    void Close();
    /**
     * Writes Point2D to the file
     */
    void WritePoint(Point2D& point, Color& color);
    /**
     * Writes PolyLine to the file
     */
    void WritePolyLine(PolyLine2D &polyline, Color& color);
    /**
     * Writes Polygon Skeleton Tree to the file with rainbow colors
     */
    void WritePolygonSkeletonTree(const PolygonSkeletonTree &st_poly);
  private:
    /**
     * Converts X coordinate of a point into SVG coordinate
     */
    float getX(Point2D& point);
    /**
     * Converts Y coordinate of a point into SVG coordinate
     * \note SVG Y coordinate is inversed
     */
    float getY(Point2D& point);

  private:
    FILE*       fFile;
    Point2D     fMin;
    Point2D     fMax;
    float       fScale;
};
} // namespace glancer
#endif // GLANCER_SVGEXPORTER_H_
