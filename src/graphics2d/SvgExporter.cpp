#include "SvgExporter.h"

#include "PolygonSkeletonTree.h"

#include <iostream> // for cout

namespace glancer {
//-------------------------------------------------------------------//
SvgExporter::Color::Color(unsigned char _r, unsigned char _g, unsigned char _b):
        r(_r), g(_g), b(_b) {
}
//-------------------------------------------------------------------//
SvgExporter::SvgExporter(Point2D& min, Point2D& max, float scale):
        fFile(NULL), fMin(min), fMax(max), fScale(scale) {
}
//-------------------------------------------------------------------//
SvgExporter::~SvgExporter(){
    Close();
}
//-------------------------------------------------------------------//
bool SvgExporter::Open(const char* filename){
    // Close the file if it was already open
    if (fFile != NULL){
        Close();
    }
    fFile = fopen(filename, "w");
    if (fFile == NULL) {
        return false;
    }
    fprintf(fFile,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n");
    float svg_width  = getX(fMax) - getX(fMin);
    float svg_height = getY(fMin) - getY(fMax);
    fprintf(fFile,"<svg width=\"%f\" height=\"%f\" ", svg_width*1.2, svg_height*1.2);
    fprintf(fFile,"viewBox=\"%f %f %f %f\" ", getX(fMin) - svg_width*0.1, getY(fMax) - svg_height*0.1, 
        svg_width*1.2, svg_height*1.2 );
    fprintf(fFile,"xmlns=\"http://www.w3.org/2000/svg\">\n");

    return true;
}
//-------------------------------------------------------------------//
void SvgExporter::Close(){
    if (fFile == NULL){
        // Do nothing if the file wasn't open
        return;
    }
    fprintf(fFile,"</svg>\n");
    fclose(fFile);
    fFile = NULL;
}
//-------------------------------------------------------------------//
void SvgExporter::WritePoint(Point2D& point, Color& color){
    if (fFile == NULL){
        // Do nothing if the file wasn't open
        return;
    }
    // <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
    float circle_radius = (getX(fMax) - getX(fMin))*0.003; // 0.3% of the width
    fprintf(fFile,"  <circle cx=\"%f\" cy=\"%f\" r=\"%f\" ", getX(point), getY(point), circle_radius);
    fprintf(fFile," style=\"stroke:rgb(%u,%u,%u);stroke-width:1;fill-opacity:0;fill:red\" />\n",
        color.r, color.g, color.b);
}
//-------------------------------------------------------------------//
void SvgExporter::WritePolyLine(PolyLine2D &polyline, Color& color){
    if (fFile == NULL){
        // Do nothing if the file wasn't open
        return;
    }
    Point2D cur_point=polyline.GetStart();
    WritePoint(cur_point, color);
    unsigned char stroke_width = 2;
    for (auto it = polyline.GetVectors().begin(); it != polyline.GetVectors().end(); ++it) {
        Point2D next_point = cur_point + *it;
        fprintf(fFile,"  <line x1=\"%f\" y1=\"%f\" ", getX(cur_point), getY(cur_point));
        fprintf(fFile,"x2=\"%f\" y2=\"%f\" ", getX(next_point), getY(next_point));
        fprintf(fFile,"style=\"stroke:rgb(%u,%u,%u);stroke-width:%u\" />\n", 
            color.r, color.g, color.b, stroke_width);
        cur_point = next_point;
    }
}
//-------------------------------------------------------------------//
void SvgExporter::WritePolygonSkeletonTree(const PolygonSkeletonTree &st_poly) {
    // This function will draw vectors of the bisectors, using Rainbow pattern
    SvgExporter::Color polyline_color(0,0,0);
    int sample_size = st_poly.GetNofFeatures();
    int ii = 0;
    float stroke_width = 0.5;
    for (auto pnt_it = st_poly.fPoints.fPoints.cbegin(); pnt_it != st_poly.fPoints.fPoints.cend(); ++pnt_it) {
        for (auto circ_index_it = pnt_it->second.mc.cbegin(); circ_index_it != pnt_it->second.mc.cend(); ++ circ_index_it){
            auto circ_it = st_poly.fCircles.find(*circ_index_it);
            if (circ_it == st_poly.fCircles.cend()) {
                // corresponding medial circle wasn not found
                continue;
            }
            // parent_pnt_it should be valid now...

            ii = pnt_it->second.feature;
            int r = ii * 255 / sample_size;
            int g = (ii * 255 * 11 / sample_size) % (255 * 2);
            int b = (ii * 255 * 5 / sample_size) % (255 * 2);
            if (b > 255) b = 255 * 2 - b;
            if ((r>250)&&(g>250)&&(b>250)) {
                r = 100;g=100;b=100; // avoid white
            }
            Point2D to_point   = circ_it->second.center;
            Point2D from_point = pnt_it->second.p;

            fprintf(fFile,"  <line x1=\"%f\" y1=\"%f\" ", getX(from_point), getY(from_point));
            fprintf(fFile,"x2=\"%f\" y2=\"%f\" ", getX(to_point), getY(to_point));
            fprintf(fFile,"style=\"stroke:rgb(%u,%u,%u);stroke-width:%f\" />\n", r, g, b, stroke_width);
        }
    }
}
//-------------------------------------------------------------------//
float SvgExporter::getX(Point2D& point){
    return (float) fScale*point.X();
}
//-------------------------------------------------------------------//
float SvgExporter::getY(Point2D &point){
    return (float) -fScale*point.Y();
}
//-------------------------------------------------------------------//
} // namespace glancer
