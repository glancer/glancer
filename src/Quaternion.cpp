#include "Quaternion.h"
#include "Matrix3D.h"

#include <cmath>    // for std::sqrt
namespace glancer {
//-------------------------------------------------------------------//
Quaternion::Quaternion() : 
        r(0,0,0), w(1.0) {

}
//-------------------------------------------------------------------//
Quaternion::Quaternion(FVector3D& _r, float _w) :
        r(_r),w(_w) {
}
//-------------------------------------------------------------------//
Quaternion::~Quaternion(){
}
//-------------------------------------------------------------------//
FVector3D Quaternion::R() const{
    return r;
}
//-------------------------------------------------------------------//
float Quaternion::W() const {
    return w;
}
//-------------------------------------------------------------------//
void Quaternion::FromMatrix (const Matrix3D& rot_matrix){
    float trace = rot_matrix[0][0] + rot_matrix[1][1] + rot_matrix[2][2];
    if (trace > 0.0) {
        float root = std::sqrt(trace + 1.0);
        w = 0.5*root;
        root = 0.5/root;
        r.x = (rot_matrix[2][1] - rot_matrix[1][2])*root;
        r.y = (rot_matrix[0][2] - rot_matrix[2][0])*root;
        r.z = (rot_matrix[1][0] - rot_matrix[0][1])*root;
    } else {
        // we'll need to select best principle axis basis (i,j,k)
        static size_t next_idx[3] = {1, 2, 0};
        size_t i = 0;
        if ( rot_matrix[1][1] > rot_matrix[0][0] )
            i = 1;
        if (rot_matrix[2][2] > rot_matrix[i][i])
            i = 2;
        size_t j = next_idx[i];
        size_t k = next_idx[k];
        float root = std::sqrt(rot_matrix[i][i] - rot_matrix[j][j] - rot_matrix[k][k] + 1.0);

        float* apk_quat[3] = {&r.x, &r.y, &r.z};
        *apk_quat[i] = 0.5* root;
        root = 0.5/root;
        w = (rot_matrix[k][j] - rot_matrix[j][k])*root;
        *apk_quat[j] = (rot_matrix[j][i] + rot_matrix[i][j])*root;
        *apk_quat[k] = (rot_matrix[k][i] + rot_matrix[i][k])*root;
    }
    Normalize();
}
//-------------------------------------------------------------------//
void Quaternion::ToMatrix (Matrix3D& rot_matrix) const {
    // note: Norm of quaterion enters matrix as multiplying by 1/norm2()
    // Everywhere where Tnn included so we can multiply by it for Tn and be norm-independent
    // It brings 3 additional divisions, but allows us not to depend on std::sqrt for renormalization
    float Tx  = r.x+r.x;    // /norm2() for non-unit Quaternions
    float Ty  = r.y+r.y;    // /norm2() for non-unit Quaternions
    float Tz  = r.z+r.z;    // /norm2() for non-unit Quaternions
    float Twx = Tx*w;
    float Twy = Ty*w;
    float Twz = Tz*w;
    float Txx = Tx*r.x;
    float Txy = Ty*r.x;
    float Txz = Tz*r.x;
    float Tyy = Ty*r.y;
    float Tyz = Tz*r.y;
    float Tzz = Tz*r.z;
    rot_matrix[0][0] = 1.0-(Tyy+Tzz);   rot_matrix[0][1] =      Txy-Twz ;   rot_matrix[0][2] =      Txz+Twy ;
    rot_matrix[1][0] =      Txy+Twz ;   rot_matrix[1][1] = 1.0-(Txx+Tzz);   rot_matrix[1][2] =      Tyz-Twx ;
    rot_matrix[2][0] =      Txz-Twy ;   rot_matrix[2][1] =      Tyz+Twx ;   rot_matrix[2][2] = 1.0-(Txx+Tyy);
}
//-------------------------------------------------------------------//
FVector3D Quaternion::operator* (const FVector3D& orig_vec) const{
    FVector3D qvec(r);
    FVector3D uv = qvec.cross(orig_vec);
    FVector3D uuv = qvec.cross(uv);
    uv *= 2.0*w;
    uuv *= 2.0;

    return orig_vec + uv + uuv;
}
//-------------------------------------------------------------------//
Quaternion Quaternion::inverse() const {
    
    float inv_norm = 1.0 / (w*w + r.norm2());
    FVector3D inv_vec(r);
    inv_vec *= -inv_norm;
    return Quaternion( inv_vec, w*inv_norm);
     
    /*
    FVector3D inv_vec(r);
    return Quaternion( inv_vec, -w);
    */
}
//-------------------------------------------------------------------//
void Quaternion::Normalize() {
    float quat_norm = w*w + r.x*r.x + r.y*r.y + r.z*r.z;
    if (quat_norm == 0) {
        return;
    }
    quat_norm = 1.0 / std::sqrt(quat_norm);
    w *= quat_norm;
    r *= quat_norm;
}
//-------------------------------------------------------------------//
} // namespace glancer
