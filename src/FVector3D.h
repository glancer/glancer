#ifndef GLANCER_FVECTOR3D_H_
#define GLANCER_FVECTOR3D_H_

namespace glancer {
    // Forward declarations
    class Vector3D;

class FVector3D {
    friend class Quaternion;
  public:
    FVector3D(float _x, float _y, float _z);
    /**
     * This will create float point vector from integer vector
     */
    FVector3D(const Vector3D &int_vector);
    ~FVector3D();
    /**
     * For accessing vector components directly
     */
    float X() const;
    float Y() const;
    float Z() const;
    /**
     * Returns cross product of this vector and another vector
     */
    FVector3D cross(const FVector3D &vec) const;
    /**
     * Dot product of this vector and another vector
     */
    float dot(const FVector3D &p) const;
    /**
     * Returns the norm^2 of the vector
     */
    float norm2() const;
    /**
     * Returns the norm of the vector
     */
    float norm() const;
    /**
     * Make unit vector out of this one
     */
    void normalize();
    /**
     * Returns the summ of this vector and another vector
     */
    FVector3D operator + (const FVector3D& vec) const;
    /** 
     * Multiplies vector by a float value
     */
    FVector3D operator * (const float f) const;
    /**
     * Multiplies vector by float value;
     */
    FVector3D& operator *= (const float f);

  private:
    float x,y,z;
};
} // namespace glancer
#endif // GLANCER_FVECTOR3D_H_
