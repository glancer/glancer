#ifndef GLANCER_POLYGONSEGMENT_H_
#define GLANCER_POLYGONSEGMENT_H_

#include <cstddef>  // for std::size_t

namespace glancer {

class PolygonSegment {
  public:
    // Apparently unordered maps do want a public constructor.
    PolygonSegment();
    PolygonSegment(std::size_t from, std::size_t to, bool foreign);
    ~PolygonSegment();
  public:
    std::size_t vertex_index[2]; // Stores from[0] and to [1] Vertex indices.
    bool isVisited; // This is used by polygon decomposition to mark a given segment as processed
    bool isForeign; // This is used for easily filtering out segments that are belonging to other polygon
};
} // namespace glancer
#endif // GLANCER_POLYGONSEGMENT_H_
