#include "Polygon.h"
#include "PolygonBentleyOttmann.h"
#include "Point2D.h"
#include "Vector2D.h"

#include <algorithm>
#include <iostream>
#include <map>

namespace glancer {
//-------------------------------------------------------------------//
Polygon::Polygon() :
        nav_segment_idx(1), nav_vert_idx(1), 
        // Indexing is starting from 1, reserving 0 to no-vertex/segment
        min(0,0), max(0,0),
        seg_summ(0,0) {
  
}
//-------------------------------------------------------------------//
Polygon::~Polygon(){
    segments.clear();
    avail_segment_idx.clear();
    vertices.clear();
    vertices_reverse.clear();
    avail_vert_idx.clear();
}
//-------------------------------------------------------------------//
size_t Polygon::AddSegment(Point2D &from, Point2D &to, bool foreign){
    size_t from_idx = AddVertex(from);
    size_t to_idx   = AddVertex(to);

    if ( from_idx == to_idx ) {
        /*
        std::cout << "Polygon: Same vertex segment [" << from_idx << "," << to_idx << "]" 
            << "FROM=[" << from.X() << ";" << from.Y() << "]"
            << "TO=["   <<   to.X() << ";" <<   to.Y() << "]"
            << std::endl; 
        */
        return 0; // NoSegment
    }
    size_t result;
    if (avail_segment_idx.size() > 0 ) {
        // Let's try to grab index from avail_vert_idx first
        result = avail_segment_idx[0];
        avail_segment_idx.erase(avail_segment_idx.begin());
    } else {
        result = nav_segment_idx;
        nav_segment_idx++;
    }
    segments.emplace(result, PolygonSegment(from_idx, to_idx, foreign) );
    // Let's update Vertices indexing too
    vertices[from_idx].AddFrom(result);
    vertices[to_idx].AddTo(result);
    // And the summ vector
    seg_summ += Vector2D(from, to);
    return result;
}
//-------------------------------------------------------------------//
void Polygon::AddPolygon(Polygon& polygon, Vector2D& offset, bool foreign) {
    for ( auto it = polygon.segments.cbegin(); it != polygon.segments.cend(); ++it){
        auto point_from = polygon.vertices.find(it->second.vertex_index[0]);
        auto point_to   = polygon.vertices.find(it->second.vertex_index[1]);
        if ((point_from == polygon.vertices.end())||(point_to == polygon.vertices.end())) {
            // Points were not found...
            continue;
        }
        Point2D from_point = point_from->second.p + offset;
        Point2D to_point = point_to->second.p + offset;
        AddSegment(from_point, to_point, foreign);
    }
}
//-------------------------------------------------------------------//
size_t Polygon::getNofVertices() const{
    return vertices.size();
}
//-------------------------------------------------------------------//
size_t Polygon::getNofSegments() const{
    return segments.size();
}
//-------------------------------------------------------------------//
Point2D& Polygon::getMin() {
    return min;
}
//-------------------------------------------------------------------//
Point2D& Polygon::getMax() {
    return max;
}
//-------------------------------------------------------------------//
Vector2D& Polygon::getSegSum() {
    return seg_summ;
}
//-------------------------------------------------------------------//
void Polygon::Decompose() {
    // clear old results
    setAllSegmentsVisited(false);
    polylines.clear();

    size_t start_segindex;
    size_t cur_segindex;
    size_t cur_vertindex;

    while ( (start_segindex = GetUnvisitedSegment()) > 0 ){
        // std::cout << "S=" << start_segindex;
        PolygonPolyLine polyline_storage;
        polyline_storage.from_vertex_index = 0;
        polyline_storage.to_vertex_index = 0;
        // We should first go backwards and find branching index or already visited index 
        // (the later would indicate that we completed loop going backward
        cur_vertindex = segments[start_segindex].vertex_index[0];
        // std::cout << " V=" << cur_vertindex;
        // We should actually start not from start_segindex, but from the index preceeding it
        if (!isVertexBranching(cur_vertindex)) {
            cur_segindex = vertices[cur_vertindex].to_segment_idx[0];
            // if we started at branching index, the following should not execute
            while (!segments[cur_segindex].isVisited) {
                segments[cur_segindex].isVisited = true;
                polyline_storage.segment_indexes.push_back(cur_segindex);
                cur_vertindex = segments[cur_segindex].vertex_index[0]; // <-- vertex index on a segment is always valid
                if (!isVertexBranching(cur_vertindex)){
                    // We have somewhere to move
                    cur_segindex = vertices[cur_vertindex].to_segment_idx[0];
                    // std::cout << "<";
                } else {
                    // We don't have further path
                    // Let's record the starting vertex index
                    polyline_storage.from_vertex_index = cur_vertindex;
                    // std::cout << " O=" << cur_vertindex;
                    break;
                }
            }
            // Don't forget to reverse after done going backwards !!!
            std::reverse( polyline_storage.segment_indexes.begin(), polyline_storage.segment_indexes.end());
        } else {
            // We started from the branching vertex, let's just record it
            polyline_storage.from_vertex_index = cur_vertindex;
            // std::cout << " B=" << cur_vertindex;
        }

        // We're done going backward

        // Now we should go forward
        cur_segindex = start_segindex;
        // If we complete the loop going backward already the following will not execute
        while (!segments[cur_segindex].isVisited){
            segments[cur_segindex].isVisited = true;
            polyline_storage.segment_indexes.push_back(cur_segindex);
            cur_vertindex = segments[cur_segindex].vertex_index[1]; // <-- vertex index on a segment is always valid
            if (!isVertexBranching(cur_vertindex)){
                // We have somewhere to move
                cur_segindex = vertices[cur_vertindex].from_segment_idx[0];
                // std::cout << ">";
            } else {
                // We don't have further path
                // Let's record the ending vertex index
                polyline_storage.to_vertex_index = cur_vertindex;
                // std::cout << " X=" << cur_vertindex;
                break;
            }
        }
        // std::cout << std::endl;

        // at the end we want to push our polyline results, even if there were only one segment
        //---->
        // Calculate the winding number and Foreign key
        // Ideally we want to sample winding number for the segment in the middle of the chain
        // If the segment size is 1 we have no other choice, but to sample the middle of this segment
        
        if (polyline_storage.segment_indexes.size() == 0) {
            // do nothing...
            continue;
        } else if (polyline_storage.segment_indexes.size() == 1){
            polyline_storage.winding_number = SegmentForeignWindingNumber(polyline_storage.segment_indexes[0]);
            polyline_storage.isForeign = segments[polyline_storage.segment_indexes[0]].isForeign;
        } else {
            size_t segment_to_sample;
            if (polyline_storage.segment_indexes.size() == 2) {
                // We want to sample at the end of the first segment
                segment_to_sample = polyline_storage.segment_indexes[0];
            } else {
                // We have 3 or more segments
                segment_to_sample = polyline_storage.segment_indexes[
                    polyline_storage.segment_indexes.size()/2];
            }
            polyline_storage.winding_number = SegmentForeignWindingNumber(segment_to_sample);
            polyline_storage.isForeign = segments[segment_to_sample].isForeign;
                
        }

        polylines.push_back(polyline_storage);
        
        std::cout << "Polyline [FN,WN] = ["
            << polyline_storage.isForeign << "," << polyline_storage.winding_number
            << "] FROM " << polyline_storage.from_vertex_index ;
        if (polyline_storage.from_vertex_index != 0) {
            std::cout << "[" << vertices[polyline_storage.from_vertex_index].p.X() << ","
                << vertices[polyline_storage.from_vertex_index].p.Y() << "]";
        }
        std::cout << " TO " << polyline_storage.to_vertex_index ;
        if (polyline_storage.to_vertex_index != 0) {
            std::cout << "[" << vertices[polyline_storage.to_vertex_index].p.X() << ","
                << vertices[polyline_storage.to_vertex_index].p.Y() << "]";
        }
        std::cout << " with size " << polyline_storage.segment_indexes.size() << std::endl;
    }
}
//-------------------------------------------------------------------//
void Polygon::SelfIntersect() {
    unsigned long tot_intersection_checks = 0;
    size_t crossing_vertex_index = 0;
    unsigned long brute_force_intersections = 0;

    // This is O(N^2) complexity... too slow
    // So will need to implement Bentley-Ottman Algo here
    /*  
    for (auto seg1_it = segments.cbegin(); seg1_it != segments.cend(); ++seg1_it) {
        for (auto seg2_it = segments.cbegin(); seg2_it != segments.cend(); ++seg2_it) {
            tot_intersection_checks ++;
            crossing_vertex_index = AddCrossingVertex(seg1_it->first, seg2_it->first);
            if (crossing_vertex_index > 0) {
                brute_force_intersections ++;
                std::cout << "BF says segments " << seg1_it->first << " and "  << seg2_it->first
                    << " are crossing at vertex " << crossing_vertex_index << std::endl;
            } 
        }
    }
    std::cout << "Brute Force: found " << brute_force_intersections << " after " 
        << tot_intersection_checks << " checks" << std::endl;
    */
   

    PolygonBentleyOttmann bo_algorithm;
    bo_algorithm.Run(*this);

}
//-------------------------------------------------------------------//
size_t Polygon::GetNofPolylines() const{
    return polylines.size();    
}
//-------------------------------------------------------------------//
PolyLine2D Polygon::ExportPolyLine(size_t polyline_number){
    if (polyline_number < 0 || polyline_number >= GetNofPolylines()){
        std::cout << "Requesting empty polyline" << std::endl;
        Point2D nopoint(0,0);
        return PolyLine2D(nopoint);
    }
    if ( polylines[polyline_number].segment_indexes.size() == 0 ) {
        std::cout << "Requesting empty polyline" << std::endl;
        Point2D nopoint(0,0);
        return PolyLine2D(nopoint);
    }
    size_t segindex=polylines[polyline_number].segment_indexes[0];
    auto seg_it = segments.find(segindex);
    if (seg_it == segments.end()){
        std::cout << "Export Polyline: can't find segment " << segindex << std::endl;
        Point2D nopoint(0,0);
        return PolyLine2D(nopoint);
    }
    auto vert1_it = vertices.find(seg_it->second.vertex_index[0]);
    if (vert1_it == vertices.end()){
        std::cout << "Export Polyline: can't find vertex " << seg_it->second.vertex_index[0] 
            << " for segment " << segindex << std::endl;
        Point2D nopoint(0,0);
        return PolyLine2D(nopoint);
    }
    PolyLine2D result(vert1_it->second.p);

    for (auto it = polylines[polyline_number].segment_indexes.begin(); 
                it != polylines[polyline_number].segment_indexes.end(); ++it) {
        seg_it = segments.find(*it);
        auto vert2_it = vertices.find(seg_it->second.vertex_index[1]);
        Vector2D vector_to_add(vert1_it->second.p, vert2_it->second.p);
        result.AddVector(vector_to_add);
        vert1_it = vert2_it;
    }
    return result;
}
//-------------------------------------------------------------------//
const long Polygon::GetPolyLineWindingNumber(size_t polyline_number) const {
    if (polyline_number < 0 || polyline_number >= GetNofPolylines()){
        std::cout << "Requesting empty polyline" << std::endl;
        return 0;
    }
    return polylines[polyline_number].winding_number;
}
//-------------------------------------------------------------------//
const bool Polygon::GetPolyLineForeignKey(size_t polyline_number) const {
    if (polyline_number < 0 || polyline_number >= GetNofPolylines()){
        std::cout << "Requesting empty polyline" << std::endl;
        return true;
    }
    return polylines[polyline_number].isForeign;
}
//-------------------------------------------------------------------//
size_t Polygon::AddVertex(Point2D &p) {
    // Let's see if the vertex is already there
    auto it = vertices_reverse.find(p);
    if (it != vertices_reverse.end()) {
        // We found the vertex point
        return it->second;
    }
    // We'll need to generate new index for this vertex
    size_t result;
    if (avail_vert_idx.size() > 0 ) {
        // Let's try to grab index from avail_vert_idx first
        result = avail_vert_idx[0];
        avail_vert_idx.erase(avail_vert_idx.begin());
    } else {
        result = nav_vert_idx;
        nav_vert_idx++;
    }
    PolygonVertex vertex(p);
    vertices.emplace(result, vertex);
    vertices_reverse.emplace(p, result);
    // Recalculate the bounding box
    if (vertices.size() == 1) {
        // This was the first point added
        min = max = p;
    } else {
        if ( p.x > max.x ) max.x = p.x;
        if ( p.y > max.y ) max.y = p.y;
        if ( p.x < min.x ) min.x = p.x;
        if ( p.y < min.y ) min.y = p.y;
    }
    return result;
}
//-------------------------------------------------------------------//
void Polygon::setAllSegmentsVisited(bool status) {
    for (auto it = segments.begin(); it != segments.end(); ++it) {
        it->second.isVisited = status;
    }
}
//-------------------------------------------------------------------//
size_t Polygon::GetUnvisitedSegment() {
    for (auto it = segments.cbegin(); it != segments.cend(); ++it) {
        if ( !it->second.isVisited )
            return it->first;
    }
    return 0;
}
//-------------------------------------------------------------------//
bool Polygon::isVertexBranching(const size_t &vertindex){
    auto vert_it = vertices.find(vertindex);
    if (vert_it == vertices.end()){
        return true;
    }

    if ((vert_it->second.from_segment_idx.size() != 1 )||(vert_it->second.to_segment_idx.size() !=1 )){   
        return true;
    }

    auto segm_from_it = segments.find(vert_it->second.from_segment_idx[0]);
    if (segm_from_it == segments.end() ) {
        std::cout << "isVertexBranching: from segment " << vert_it->second.from_segment_idx[0] 
            << " does not exist" << std::endl;
        return true;
    }
    auto segm_to_it = segments.find(vert_it->second.to_segment_idx[0]);
    if (segm_to_it == segments.end() ) {
        std::cout << "isVertexBranching: to segment " << vert_it->second.to_segment_idx[0]
            << " does not exist" << std::endl;
        return true;
    }

    if (segm_from_it->second.isForeign != segm_to_it->second.isForeign) {
        return true;
    }
    return false;
}
//-------------------------------------------------------------------//
size_t Polygon::AddCrossingVertex(const size_t &segindex1, const size_t &segindex2){
    if (segindex1 == 0 || segindex2 == 0) {
        // Can't cross no indicies
        return 0;
    }
    if (segindex1 == segindex2){
        // Can't cross segment with itself
        return 0;
    }
    // Get the point indicies first and see if they make sense
    const size_t pointA_index = segments[segindex1].vertex_index[0];
    if (pointA_index == 0) {
        // Can't do nopoint
        return 0;
    }
    const size_t pointB_index = segments[segindex1].vertex_index[1];
    if (pointB_index == 0 || pointA_index == pointB_index) {
        // Can't do the same point to the same point segment
        return 0;
    }
    const size_t pointC_index = segments[segindex2].vertex_index[0];
    if (pointC_index == 0 || pointA_index == pointC_index || pointB_index == pointC_index) {
        //Hmm this should never happen but just in case
        return 0;
    }
    const size_t pointD_index = segments[segindex2].vertex_index[1];
    if (pointD_index == 0 || pointA_index == pointD_index || pointB_index == pointD_index || pointC_index == pointD_index ) {
        return 0;
    }
    // All of the checks above are not necessary so should be commented out after debugging
    // We're just testing that all points are different this way
    // Let's first get point coordinates:
    const Point2D pointA = vertices[pointA_index].p;
    const Point2D pointB = vertices[pointB_index].p;
    Point2D pointC = vertices[pointC_index].p;
    const Point2D pointD = vertices[pointD_index].p;
    // Then check if their bounding boxes collide. Important
    // We don't want <= on these, because we want to be able to track cases like this
    //     |  <- new segment is passing throug existing vertex
    // ----o----
    //     |
    //So returning existing point as an intersection point should be valid in this case
    // First On Xaxis
    if ( std::max(pointA.X(),pointB.X()) < std::min(pointC.X(),pointD.X()) ) {
        // Corresponds to A..B C..D case
        return 0;
    }
    if ( std::max(pointC.X(),pointD.X()) < std::min(pointA.X(),pointB.X()) ) {
        // Corresponds to C..D A..B case;
        return 0;
    }
    // Then on Yaxis
    if ( std::max(pointA.Y(),pointB.Y()) < std::min(pointC.Y(),pointD.Y()) ) {
        // Corresponds to A..B C..D case
        return 0;
    }
    if ( std::max(pointC.Y(),pointD.Y()) < std::min(pointA.Y(),pointB.Y()) ) {
        // Corresponds to C..D A..B case;
        return 0;
    }
    // At this point bounding boxes are colliding, so there is some probability that segments will intersect
    // Line AB represented as a1x + b1y = c1
    long a1 = pointB.Y() - pointA.Y();
    long b1 = pointA.X() - pointB.X();

    // Line CD represented as a2x + b2y = c2
    long a2 = pointD.Y() - pointC.Y();
    long b2 = pointC.X() - pointD.X();

    long det = a1*b2-a2*b1;
    if (det == 0){
        // The segments are parallel
        // We should test here if parallel segments do overlap
        Vector2D inters_vec(pointA, pointC);
        Vector2D seg1_vec(pointA, pointB);
        if ((inters_vec.dot(seg1_vec) > 0)&&(inters_vec.cross(seg1_vec) == 0) ) {
            // Vectors are pointing in the same direction and laying on the same line
            long inters_vec_norm2 = inters_vec.norm2();
            if ((inters_vec_norm2 > 0)&&(inters_vec_norm2 < seg1_vec.norm2())){
                // We've got pointC on AB segment :)
                return AddVertex(pointC);
            }
        }
        return 0;
    }
    long c1 = a1*pointA.X() + b1*pointA.Y();
    long c2 = a2*pointC.X() + b2*pointC.Y();
    Point2D point((b2*c1 - b1*c2)/det, (a1*c2 - a2*c1)/det);
    // Check if the resulting point actually is on both segments
    if ( (std::min(pointA.X(), pointB.X()) > point.X()) || ( point.X() > (std::max(pointA.X(), pointB.X()))) ){
        // Corresponds to x A..B x
        return 0;
    }
    if ( (std::min(pointA.Y(), pointB.Y()) > point.Y()) || ( point.Y() > (std::max(pointA.Y(), pointB.Y()))) ){
        // Corresponds to y A..B y
        return 0;
    }
    if ( (std::min(pointC.X(), pointD.X()) > point.X()) || ( point.X() > (std::max(pointC.X(), pointD.X()))) ){
        // Corresponds to x C..D x
        return 0;
    }
    if ( (std::min(pointC.Y(), pointD.Y()) > point.Y()) || ( point.Y() > (std::max(pointC.Y(), pointD.Y()))) ){
        // Corresponds to y C..D y
        return 0;
    }
    // We also don't want to report crossing if point is exactly the same as B or D
    if ((point == pointB)||(point == pointD)) {
        return 0;
    }
    // At this point it's safe to inject this point
    return AddVertex(point);
}
//-------------------------------------------------------------------//
void Polygon::AddCrossingSegment(const size_t &segindex, const std::vector<size_t> &vertindex){
    // Let's first find the segment
    auto segment_it = segments.find(segindex);
    if (segment_it == segments.end()){
        std::cout << "Can't find segment " << segindex << std::endl;
        return;
    }
    // Then we'll need to find vertices for this segment
    auto vert_begin_it = vertices.find(segment_it->second.vertex_index[0]);
    if (vert_begin_it == vertices.end()){
        std::cout << "Can't find beginning vertex " << segment_it->second.vertex_index[0] 
            << " for segment " << segindex << std::endl;
        return;
    }
    size_t start_vert_index = vert_begin_it->first;
    // This is the vector having norm2() of a vector from the beginning of the segment to the end of each vertex
    // It needs to be sorted in increasing order
    std::vector<std::pair<long, size_t>> distance_index;
    for (auto it = vertindex.begin(); it != vertindex.end(); ++it) {
        // We don't want to add 0 length
        if (start_vert_index == *it) {
            std::cout << "...Can't add zero vector between vertices " << start_vert_index << " and " << *it << std::endl;
            continue;
        }
        Vector2D vector_to_add(vertices[start_vert_index].p, vertices[*it].p);
        distance_index.emplace_back(vector_to_add.norm2(), *it);
    }
    std::sort(distance_index.begin(), distance_index.end());
   
    size_t cur_vertex_index = start_vert_index;
    std::vector <size_t> added_segments_idx;
    for (auto it = distance_index.begin(); it != distance_index.end(); ++it) {
        if ( cur_vertex_index == it->second ){
            std::cout << "...Can't add nosegment from " << cur_vertex_index << " to " << it->second << std::endl;
            continue;
        }
        // Adding segment will invalidate segment_it, so we shouldn't use it anymore
        size_t added_segment = AddSegment(vertices[cur_vertex_index].p, vertices[it->second].p, segments[segindex].isForeign);
        if (added_segment > 0) {
            added_segments_idx.push_back(added_segment);
        } else {
            std::cout << "Add Segment returned 0" << std::endl;
            continue;
        }
        cur_vertex_index = it->second;
    }
    // We'll need to fix the segment we began with changing from for the segment itself
    // And removing it from vertex we started with and adding it to the vertex we end up with

    if (cur_vertex_index == start_vert_index) {
        // Do nothing in this case
        return;
    }
    if ((segindex == 0)||(start_vert_index == 0)||(cur_vertex_index ==0)){
        std::cout << "Add Crossing Vertex: 0 index " << segindex << "," << start_vert_index << "," << cur_vertex_index << std::endl;
        return;
    }
    segments[segindex].vertex_index[0] = cur_vertex_index;
    vertices[start_vert_index].RemoveFrom(segindex);
    vertices[cur_vertex_index].AddFrom(segindex);


    added_segments_idx.push_back(segindex);
}
//-------------------------------------------------------------------//
long Polygon::PointWindingNumber(const Point2D& point, bool foreign_key) const{
    long result = 0;
    for (auto seg_it = segments.cbegin(); seg_it != segments.cend(); ++seg_it){
        // Exclude foreign segments from calculation
        if ( seg_it->second.isForeign == foreign_key ) {
            // Do not include segments with the same foreign key
            continue;
        }
        auto vert_from_it = vertices.find(seg_it->second.vertex_index[0]);
        if (vert_from_it == vertices.end()) {
            std::cout << "Point WN: from vertindex " << seg_it->second.vertex_index[0] 
                << " of segment " << seg_it->first << std::endl;
            continue;
        }
        auto vert_to_it   = vertices.find(seg_it->second.vertex_index[1]);
        if (vert_to_it == vertices.end()) {
            std::cout << "Point WN: to vertindex " << seg_it->second.vertex_index[1] 
                << "of segment " << seg_it->first << std::endl;
            continue;
        }
        if (vert_from_it->second.p.Y() <= point.Y() ) {
            if (vert_to_it->second.p.Y() > point.Y() ) {
                // Upward going segment
                Vector2D seg_vec(vert_from_it->second.p, vert_to_it->second.p);
                Vector2D chk_vec(vert_from_it->second.p, point);
                if ( seg_vec.cross(chk_vec) > 0) {
                    ++result;
                }
            }
        } else if (vert_to_it->second.p.Y() <= point.Y()) {
            // Downward going segment
            Vector2D seg_vec(vert_from_it->second.p, vert_to_it->second.p);
            Vector2D chk_vec(vert_from_it->second.p, point);
            if ( seg_vec.cross(chk_vec) < 0) {
                --result;
            }
        }
    } // end of segments loop
    return result;
}
//-------------------------------------------------------------------//
long Polygon::SegmentForeignWindingNumber(const size_t &segindex) const {
    auto seg_it = segments.find(segindex);
    if (seg_it == segments.end())
        return 0;
    auto from_vert_it = vertices.find(seg_it->second.vertex_index[0]);
    if (from_vert_it == vertices.end()) {
        std::cout << "Segment WN: from vertex " << seg_it->second.vertex_index[0] 
            << " doesn't exist for segment " << segindex << std::endl;
        return 0;
    }
    auto to_vert_it   = vertices.find(seg_it->second.vertex_index[1]);
    if (to_vert_it == vertices.end()) {
        std::cout << "Segment WN: to vertex " << seg_it->second.vertex_index[1] 
            << " doesn't exist for segment " << segindex << std::endl;
        return 0;
    }
    // The point for a given segment is taken midway, to avoid collision with foreign segments
    Vector2D offset_vector(from_vert_it->second.p, to_vert_it->second.p);
    Point2D middle_point(from_vert_it->second.p + (offset_vector/2));
    return PointWindingNumber(middle_point, seg_it->second.isForeign);
}
//-------------------------------------------------------------------//
} // namespace glancer
