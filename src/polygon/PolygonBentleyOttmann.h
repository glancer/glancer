#ifndef GLANCER_POLYGONBENTLEYOTTMANN_H_
#define GLANCER_POLYGONBENTLEYOTTMANN_H_

namespace glancer {
// Forward declarations
class Polygon;

/**
 * This is the structure for keeping ordered list of segments
 */
class PolygonBentleyOttmannSegment{
  public:
    PolygonBentleyOttmannSegment();
    PolygonBentleyOttmannSegment(long _y, size_t _above, size_t _below);
  public:
   long y;          //< Y coordinate of the segment 
   size_t above;    //< Index of the segment above this segment (0 if there is no segment above)
   size_t below;    //< Index of the segment below this segment (0 if there is no segment below)
};
/** 
 * This is the class for Bentley-Ottmann Event
 */
class PolygonBentleyOttmannEvent{
  public:
    PolygonBentleyOttmannEvent(Point2D& _p, size_t seg_index1, size_t seg_index2);
  public:
    Point2D p;              // The Full Coordinate of the event. It will be kept sorted by p.X() first and then p.Y()
    size_t  seg_index[2];   // In case of segment leaving or entering seg_index[0] is used (seg_index[1] should be zero)
                            // In case of intersection point event, both seg_index[0] and segindex[1] are used.
};
/**
 * This class implements Bentley-Ottmann
 */
class PolygonBentleyOttmann {
  public:
    PolygonBentleyOttmann();
    ~PolygonBentleyOttmann();

    /**
     * Runs Bentley-Ottmann algorithm on a polygone
     * The polygon will be changed as the result (new vertexes and new segments are created
     * and old segments are removed
     */
    void Run(Polygon &polygon);
  private:
    /**
     * This function will copy vertices and segments data from polygon into fEventQueue
     */
    void InitializeEventQueue(Polygon &polygon);
    /**
     * This function sorts the fEventQueue based on X coordinate of the point first and then Y coordinate (if X is the same)
     */
    void SortEventQueue();
    /**
     *  Returns index of the segment above a given segment.
     *  returns 0 if there is no segment above.
     */
    size_t GetSegmentAbove(size_t segment);
    /**
     *  Returns index of the segment below a given segment.
     *  returns 0 if there is no segment.
     */
    size_t GetSegmentBelow(size_t segment);
    /**
     * Returns true if the segment is in the data structure (fSegments).
     * Used in the event queue processing to distinguish betweeen segment entering or leaving
     */
    bool isSegmentPresent (size_t segment);
    /**
     * Adds segment with a given index and Y coordinate to internal data structure (fSegments)
     */
    void AddSegment(size_t segment, long y);
    /**
     * Removes segment with a given index from internal data structure (fSegments)
     */
    void RemoveSegment(size_t segment);
    /**
     * Swaps segments withe a given indicies
     */
    void SwapSegments(size_t segment1, size_t segment2);
    /**
     * Tries to intersect two given segments in polygon and handles internal datastructure in case there was an intersection
     */
    void Intersect(Polygon &polygon, size_t segment1, size_t segment2);
  private:
    /**
     * This is is defined as maximum number of segments connected to one vertex in polygon.
     */
    size_t  fMaxVertexMultiplicity;
    /**
     * Statistics collection variables
     */
    unsigned long fNofIntersectionChecks; //< Total number of intersection checks
    unsigned long fNofIntersectionPoints; //< Total number of intersection points detected
    
    /**
     * Store the events that need to be processed There are 3 types of the event
     * 1 -> Segment entering
     * 2 -> Segment leaving
     * 3 -> Intersection
     */
    std::vector<PolygonBentleyOttmannEvent> fEventQueue;
    /**
     * Stores segments intersection events that need to be injected back into fEventQueue
     */
    std::vector<PolygonBentleyOttmannEvent> fEventBuffer;
    //* Stores segment index as the key and information about Ycoordinate, segment indicies of above and below segment
    std::unordered_map <size_t, PolygonBentleyOttmannSegment> fSegments;
    // Stores segment index as the key and the list of vertices indicies that were result of crossing
    // this particular segment with the other segments.
    // This is what actually will be used for connecting resulting intersection vertices with segments
    std::unordered_map <size_t, std::vector<size_t>> fVertices;
    //* Stores Intersection map, i.e. segment id vs the other segment ids it was intersecting with
    // It's a temporary: Every time particular segment leaves, the corresponding entry is deleted
    // so it should not be used for any other kind of checks.
    std::unordered_map <size_t, std::vector<size_t>> fIntersectionMap;
};
} // namespace glancer
#endif // GLANCER_POLYGONBENTLEYOTTMANN_H_
