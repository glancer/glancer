#include <iostream> // for cout
#include <cmath>    // for std::sqrt
#include <algorithm>    // for std::sort

#include "PolygonSkeletonTree.h"

namespace glancer {
//===================================================================//
PolygonSTPointCloud::PolygonSTPointCloud() :
        fNavPointIndex(1),
        fKDTree(2 /*dim*/, (*this), nanoflann::KDTreeSingleIndexAdaptorParams(10 /* max leaf */)) {
}
//-------------------------------------------------------------------//
PolygonSTPointCloud::~PolygonSTPointCloud() {
}
//-------------------------------------------------------------------//
void PolygonSTPointCloud::SetAllPointsVisited(bool visited_flag){
    for (auto pnt_it = fPoints.begin(); pnt_it != fPoints.end(); ++pnt_it){
        pnt_it->second.isVisited = visited_flag;
    }
}
//-------------------------------------------------------------------//
size_t PolygonSTPointCloud::GetRandomUnvisitedPointIndex() {
    for (auto pnt_it = fPoints.cbegin(); pnt_it != fPoints.cend(); ++pnt_it){
        if (!pnt_it->second.isVisited) 
            return pnt_it->first;
    }
    return 0;
}
//-------------------------------------------------------------------//
void PolygonSTPointCloud::Add(size_t index, const PolygonSTPoint& point){
    fPointsMap.push_back(index);
    fPoints.emplace(index, point);
}
//-------------------------------------------------------------------//
void PolygonSTPointCloud::BuildIndex(){
    fKDTree.buildIndex();
}
//-------------------------------------------------------------------//
size_t PolygonSTPointCloud::GetClosestPointIndexExcluding(const Point2D& sample, size_t excluding){
    const long query_pt[2] = { sample.X(), sample.Y()};
    size_t num_results = 2;
    std::vector<size_t> ret_index(num_results);
    std::vector<long> out_dist_sqr(num_results);

    num_results = fKDTree.knnSearch(&query_pt[0], num_results, &ret_index[0], &out_dist_sqr[0]);
    // In case of less points in the tree than requested:
    ret_index.resize(num_results);
	out_dist_sqr.resize(num_results);

    // By default the result is sorted by out_dist_sqr

    // std::cout << "knnSearch(): num_results=" << num_results << std::endl;
    // for (size_t i = 0; i < num_results; i++)
    //    std::cout << "idx["<< i << "]=" << ret_index[i] << " dist["<< i << "]=" << std::sqrt(out_dist_sqr[i]) << std::endl;
    // Zero result shouldn't be possible, but just in case
    if (num_results == 0)
        return 0;

    if (excluding == 0) {
        // no exclusion so return index 0
        return fPointsMap[ret_index[0]];
    }
    // There is an exclusion, so just see if 0 index is the excluded index
    if (fPointsMap[ret_index[0]] == excluding) {
        // first index is the exclusion, so we would like to see if we have second index and return it
        if (num_results == 1){
            // There was only 1 point that we were asked to be excluded
            return 0;
        } else {
            return fPointsMap[ret_index[1]];
        }
    } else {
        // first index is what we need
        return fPointsMap[ret_index[0]];
    }
}
//===================================================================//
PolygonSkeletonTree::PolygonSkeletonTree(long max_z_distance, long resample_distance) :
        fNavCircle(1),
        fMaxZ(max_z_distance), fResampleDist(resample_distance),
        fNofFeatures(0) {
}
//-------------------------------------------------------------------//
PolygonSkeletonTree::~PolygonSkeletonTree() {
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::LoadBaseFromDecomposedPolygon(const Polygon& polygon){
    fNofFeatures = 0;
    for (auto feature_it = polygon.polylines.cbegin(); feature_it != polygon.polylines.cend(); ++feature_it) {
        if (feature_it->from_vertex_index != feature_it->to_vertex_index) {
            std::cout << "PST: Polygon has non-closed features" << std::endl;
            continue;
        }
        fNofFeatures++;
        // Let's fill points array
        // We'll need to link points in the point cloud, for this we want to know which point is first 
        size_t first_feature_point_index = fPoints.fNavPointIndex;
        long prev_accum_length = fResampleDist/2;
        for (auto seg_it = feature_it->segment_indexes.cbegin(); seg_it != feature_it->segment_indexes.cend(); ++seg_it) {
            auto polyseg_it = polygon.segments.find(*seg_it);
            if (polyseg_it == polygon.segments.cend()) {
                std::cout << "PST: Can't find segment " << *seg_it << std::endl;
                continue;
            }
            auto prev_vertex_it = polygon.vertices.find(polyseg_it->second.vertex_index[0]);
            if (prev_vertex_it == polygon.vertices.cend()) {
                std::cout << "PST: Can't find vertex " << polyseg_it->second.vertex_index[0] << std::endl;
                continue;
            }
            auto next_vertex_it = polygon.vertices.find(polyseg_it->second.vertex_index[1]);
            if (next_vertex_it == polygon.vertices.cend()) {
                std::cout << "PST: Can't find next vertex " << polyseg_it->second.vertex_index[1] << std::endl;
                continue;
            }
            // At this point we should have valid iterators
            InsertSamplePoints(prev_vertex_it->second.p, next_vertex_it->second.p, prev_accum_length, fNofFeatures);

        } // for cycle for all feature segments
        // We'll need to fix pointers for first and last inserted points
        // The last inserted index will be fPoints.fNavPointIndex - 1 and the first inserted index will be first_feature_point_index
        auto first_point_it = fPoints.fPoints.find(first_feature_point_index);
        if (first_point_it == fPoints.fPoints.end()){
            std::cout << "PST: Should never happen, unable to find first point " << std::endl;
            continue;
        }
        auto last_point_it = fPoints.fPoints.find(fPoints.fNavPointIndex - 1);
        if (last_point_it == fPoints.fPoints.end()){
            std::cout << "PST: Should never happen, unable to find last point " << std::endl;
            continue;
        }
        first_point_it->second.prev =  last_point_it->first;
        last_point_it->second.next  = first_point_it->first;
    }
    // Recompute bisector vectors for all the points
    RecomputeBisectorVectors(); 
    // Generate medial axis circles for each point
    GenerateMedialAxisCircles();
    // Build KD-Tree index for points
    fPoints.BuildIndex();
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::Construct() {
    // Iteratively fit medial axis circles
    for (auto circ_it = fCircles.begin(); circ_it != fCircles.end(); ++circ_it) {
        FitMedialCircleToPolygon(circ_it->second);
    }
    AddMedialCirclesConnectionToPoints();
    SortMedialCirclesConnectionToPoints();
    // TODO: insert additional medial circles here to better sample medial line
    GenerateStats();
}
//-------------------------------------------------------------------//
const size_t PolygonSkeletonTree::GetNofFeatures() const {
    return fNofFeatures;
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::AddMedialCirclesConnectionToPoints() {
    for (auto circ_it = fCircles.begin(); circ_it != fCircles.end(); ++circ_it) {
        auto pnt_it = fPoints.fPoints.find( circ_it->second.parent_idx );
        if (pnt_it != fPoints.fPoints.end()){
            // We'll need to see if the connection was already added
            auto circ_idx_it = std::find(pnt_it->second.mc.begin(), pnt_it->second.mc.end(), circ_it->first);
            if (circ_idx_it == pnt_it->second.mc.end()) {
                // Index wasn't found, so push it back
                pnt_it->second.mc.push_back(circ_it->first);
            }
        }
        // Do the same for participant, 0 should be treated right since it should not exist
        pnt_it = fPoints.fPoints.find( circ_it->second.partic_idx );
        if (pnt_it != fPoints.fPoints.end()){
            // We'll need to see if the connection was already added
            auto circ_idx_it = std::find(pnt_it->second.mc.begin(), pnt_it->second.mc.end(), circ_it->first);
            if (circ_idx_it == pnt_it->second.mc.end()) {
                // Index wasn't found, so push it back
                pnt_it->second.mc.push_back(circ_it->first);
            }
        }
    }
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::SortMedialCirclesConnectionToPoints() {
    for (auto pnt_it = fPoints.fPoints.begin(); pnt_it != fPoints.fPoints.end(); ++pnt_it){
        if (pnt_it->second.mc.size() < 2) {
            continue;
        }
        SortMedialCirclesConnectionToPoint(pnt_it->second);
    }
}
//-------------------------------------------------------------------//
size_t PolygonSkeletonTree::FitMedialCircleToPolygon(PolygonSTMedialCircle& circle){
    auto parent_pnt_it = fPoints.fPoints.find(circle.parent_idx);
    if (parent_pnt_it == fPoints.fPoints.cend()) {
        std::cout << "PST: Can't find parent point " << circle.parent_idx << std::endl;
        return 0;
    }
    do {
        // First let's find the closest point to the center of this circle, excluding the parent index
        size_t closest_point_index = fPoints.GetClosestPointIndexExcluding(circle.center, parent_pnt_it->first);
        auto closest_pnt_it = fPoints.fPoints.find(closest_point_index);
        if (closest_pnt_it == fPoints.fPoints.cend()) {
            std::cout << "PST: Can't find the closest point " << closest_point_index << std::endl;
            return 0;
        }
        // Let's see if the point is within old radius
        Vector2D old_dist_vec(circle.center, closest_pnt_it->second.p);
        if (old_dist_vec.norm2() > circle.radius*circle.radius){
            // std::cout << "PST: closest point is outside of the old radius" << std::endl;
            return 0;
        }

        // Let's calculate the new radius: The formula is given by (|d|^2 * |n|)/(2*(d dot n))
        // where d is vector from parent point to closest point and n is normal vector for parent point
        Vector2D dist_vec(parent_pnt_it->second.p, closest_pnt_it->second.p);
        long dist_dotproduct = parent_pnt_it->second.b.dot(dist_vec);
        // Don't fo anything if dot product is zero?
        if (dist_dotproduct == 0) {
            // std::cout << "PST: Dot product is zero, do nothing " << std::endl;
            return 0;
        }
        // Assuming the normals are normalized to |n| = fMaxZ
        float new_circle_radius = (float) std::abs(fMaxZ * dist_vec.norm2() / ( 2.0 * dist_dotproduct));
        // Do nothing if the new radius is actually larger than the current one
        if (new_circle_radius >= (float) circle.radius) {
            // std::cout << "PST: New radius is larger, do nothing" << std::endl;
            return 0;
        }
    
        Vector2D center_offset_vec(parent_pnt_it->second.p, circle.center);
        if (circle.radius != 0) {
            center_offset_vec *= (float) new_circle_radius/ circle.radius;
        } else {
            center_offset_vec *= (long) 0;
        }
        // And update the center and radius here
        circle.partic_idx = closest_point_index;
        circle.center = parent_pnt_it->second.p + center_offset_vec;
        circle.radius = center_offset_vec.norm();
    } while (true);

    return 0; // for now
}
//-------------------------------------------------------------------//
bool PolygonSkeletonTree::FindIntersectionPoint(const Point2D& pointA, const Point2D& pointB, 
        const Point2D& pointC, const Point2D& pointD, Point2D &result) const {
    // Check BB first On Xaxis
    if ( std::max(pointA.X(),pointB.X()) < std::min(pointC.X(),pointD.X()) ) {
        // Corresponds to A..B C..D case
        return false;
    }
    if ( std::max(pointC.X(),pointD.X()) < std::min(pointA.X(),pointB.X()) ) {
        // Corresponds to C..D A..B case;
        return false;
    }
    // Then check BB on Yaxis
    if ( std::max(pointA.Y(),pointB.Y()) < std::min(pointC.Y(),pointD.Y()) ) {
        // Corresponds to A..B C..D case
        return false;
    }
    if ( std::max(pointC.Y(),pointD.Y()) < std::min(pointA.Y(),pointB.Y()) ) {
        // Corresponds to C..D A..B case;
        return false;
    }
    // Line AB represented as a1x + b1y = c1
    long a1 = pointB.Y() - pointA.Y();
    long b1 = pointA.X() - pointB.X();

    // Line CD represented as a2x + b2y = c2
    long a2 = pointD.Y() - pointC.Y();
    long b2 = pointC.X() - pointD.X();

    long det = a1*b2-a2*b1;
    if (det == 0){
        // The segments are parallel
        return false;
    }
    long c1 = a1*pointA.X() + b1*pointA.Y();
    long c2 = a2*pointC.X() + b2*pointC.Y();
    Point2D point((b2*c1 - b1*c2)/det, (a1*c2 - a2*c1)/det);
    // Check if the resulting point actually is on both segments
    if ( (std::min(pointA.X(), pointB.X()) > point.X()) || ( point.X() > (std::max(pointA.X(), pointB.X()))) ){
        // Corresponds to x A..B x
        return false;
    }
    if ( (std::min(pointA.Y(), pointB.Y()) > point.Y()) || ( point.Y() > (std::max(pointA.Y(), pointB.Y()))) ){
        // Corresponds to y A..B y
        return false;
    }
    if ( (std::min(pointC.X(), pointD.X()) > point.X()) || ( point.X() > (std::max(pointC.X(), pointD.X()))) ){
        // Corresponds to x C..D x
        return false;
    }
    if ( (std::min(pointC.Y(), pointD.Y()) > point.Y()) || ( point.Y() > (std::max(pointC.Y(), pointD.Y()))) ){
        // Corresponds to y C..D y
        return false;
    }
    // We also don't want to report crossing if point is exactly the same as B or D
    if ((point == pointB)||(point == pointD)) {
        return false;
    }
    // At this point it's safe to inject this point
    result = point;
    return true;

}
//-------------------------------------------------------------------//
Vector2D PolygonSkeletonTree::GetBisectorVector(const Point2D& left, const Point2D& middle, const Point2D& right){
    Vector2D left_normal (left.Y() - middle.Y(),  middle.X() - left.X());
    Vector2D right_normal(middle.Y() - right.Y(), right.X() - middle.X());
    float left_norm   = left_normal.norm();
    if (left_norm == 0)
        left_norm = 1;
    float right_norm = right_normal.norm();
    if (right_norm == 0)
        right_norm = 1;
    Vector2D bisector_vector  = (left_norm * right_normal) + (right_norm * left_normal);
    float bisector_norm = bisector_vector.norm();
    if (bisector_norm == 0)
        bisector_norm = 1;
    bisector_vector *= (float) fMaxZ / bisector_norm;
    return bisector_vector;
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::InsertSamplePoints(const Point2D& from_pnt, const Point2D& to_pnt, long &accum_length, size_t feature){
    Vector2D segment_vector(from_pnt, to_pnt);
    Vector2D accum_vector(to_pnt, from_pnt);
    long segment_vector_norm = segment_vector.norm();
    if (segment_vector_norm == 0) {
        // should never happen
        std::cout << "Segment norm is zero" << std::endl;
        return;
    }
    /*
    std::cout << "--> InsertSamplePoints from [" << from_pnt.X() << "," << from_pnt.Y() 
        << "] to [" << to_pnt.X() << "," << to_pnt.Y() << "] with resample distance " 
        << fResampleDist << std::endl;
    */
    accum_vector *= (float) accum_length/segment_vector_norm;
    segment_vector -= accum_vector;
    segment_vector_norm += accum_length;
    Point2D current_point = from_pnt + accum_vector;
    
    segment_vector *= (float) fResampleDist/segment_vector_norm;
    long nof_inserted_points = (long) segment_vector_norm / fResampleDist;
    for (long ii = 0; ii < nof_inserted_points; ++ii) {
        current_point = current_point + segment_vector;
        // std::cout << "...Inserting point [" << current_point.X() << "," << current_point.Y() << "]" << std::endl;
        PolygonSTPoint st_point {fPoints.fNavPointIndex - 1, fPoints.fNavPointIndex + 1, feature,
            current_point, Vector2D(0,0), false};
        fPoints.Add(fPoints.fNavPointIndex, st_point);
        fPoints.fNavPointIndex++;
    }
    accum_length = (long) segment_vector_norm % fResampleDist;
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::RecomputeBisectorVectors(){
    for (auto pnt_it = fPoints.fPoints.begin(); pnt_it != fPoints.fPoints.end(); ++pnt_it){
        auto prev_pnt_it = fPoints.fPoints.find(pnt_it->second.prev);
        if (prev_pnt_it == fPoints.fPoints.cend()){
            std::cout << "PST: Should never happen, unable to find prev point " << std::endl;
            continue;
        }
        auto next_pnt_it = fPoints.fPoints.find(pnt_it->second.next);
        if (next_pnt_it == fPoints.fPoints.cend()){
            std::cout << "PST: Should never happen, unable to find next point " << std::endl;
            continue;
        }
        pnt_it->second.b = GetBisectorVector(prev_pnt_it->second.p, pnt_it->second.p, next_pnt_it->second.p);
    }
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::GenerateMedialAxisCircles(){
    for (auto pnt_it = fPoints.fPoints.cbegin(); pnt_it != fPoints.fPoints.cend(); ++pnt_it){
        Point2D circle_center = pnt_it->second.p + pnt_it->second.b;
        long circle_radius = fMaxZ;
        PolygonSTMedialCircle st_circle{circle_center, circle_radius, 
            pnt_it->first, 0, // Initially there is no participant
            false // visited flag
            };
        fCircles.emplace(pnt_it->first, st_circle);
    }
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::SortMedialCirclesConnectionToPoint(PolygonSTPoint& point){
    // For the base vector we'll need to find prev or next point
    Vector2D base_vector_x(0,0);
    auto next_pnt_it = fPoints.fPoints.find(point.next);
    if (next_pnt_it == fPoints.fPoints.cend()){
        // Next point wasn't found, let's use prev point for the base vector
        auto prev_pnt_it = fPoints.fPoints.find(point.prev);
        if (prev_pnt_it == fPoints.fPoints.cend()){
            std::cout << "PST: Should never happen, both prev or next point were not found" << std::endl;
            return;
        } else {
            // Prev point was found and no next point
            base_vector_x = Vector2D(point.p, prev_pnt_it->second.p);
        }
    } else {
        // Next point was found
        base_vector_x = Vector2D(point.p, next_pnt_it->second.p);
    }
    Vector2D base_vector_y(-base_vector_x.Y(), base_vector_x.X());

    // Now we will sort all vector by quadrants they fall into
    std::array<std::vector<size_t>, 4> quadrant_mcs;

    for (size_t ii = 0; ii < point.mc.size(); ++ii){
        auto circ_it = fCircles.find(point.mc[ii]);
        if (circ_it == fCircles.cend()) {
            // medial circle wasn't found
            continue;
        }
        Vector2D rib_vector(point.p, circ_it->second.center);
        // We want a sign of dot product for rib vector and base_x,y vectors
        long dot_prod_x = rib_vector.dot(base_vector_x);
        long dot_prod_y = rib_vector.dot(base_vector_y);
        if (dot_prod_x >= 0) {
            // Can be quadrant 0 or quadrant 3
            if (dot_prod_y >= 0){
                // quadrant 0
                quadrant_mcs[0].push_back(point.mc[ii]);
            } else {
                // quadrant 3
                quadrant_mcs[3].push_back(point.mc[ii]);
            }
        } else {
            // Can be quadrant 1 or quadrant 2
            if (dot_prod_y >= 0){
                // quadrant 1
                quadrant_mcs[1].push_back(point.mc[ii]);
            } else {
                // quadrant 2
                quadrant_mcs[2].push_back(point.mc[ii]);
            }
        }
    }
    // At this point we should have all medial circles indicies in quadrants
    // we'll need to sort each quadrant based on the sign of cross product
    for (size_t ii = 0; ii < 4; ++ii){
        if (quadrant_mcs[ii].size() < 2)
            continue;
        size_t nof_swaps;
        do {
            nof_swaps = 0;
            for (size_t jj = 1; jj < quadrant_mcs[ii].size(); ++jj){
                auto prev_circ_it = fCircles.find(quadrant_mcs[ii][jj-1]);
                if (prev_circ_it == fCircles.cend()) {
                    // prev circle wasn't found
                    continue;
                }
                auto next_circ_it = fCircles.find(quadrant_mcs[ii][jj]);
                if (next_circ_it == fCircles.cend()) {
                    // next circle wasn't found
                    continue;
                }
                // Now the iterators should be all set, let's construct vectors and test their cross-product
                Vector2D prev_rib_vector(point.p, prev_circ_it->second.center);
                Vector2D next_rib_vector(point.p, next_circ_it->second.center);
                if (next_rib_vector.cross(prev_rib_vector) < 0) {
                    std::swap(quadrant_mcs[ii][jj-1], quadrant_mcs[ii][jj]);
                    nof_swaps++;
                }
            }
        } while (nof_swaps > 0);
    }
    // We wan Q3 to be the first and Q0 to be the last...
    point.mc.clear();
    
    point.mc.insert(point.mc.end(), quadrant_mcs[3].begin(), quadrant_mcs[3].end());
    point.mc.insert(point.mc.end(), quadrant_mcs[2].begin(), quadrant_mcs[2].end());
    point.mc.insert(point.mc.end(), quadrant_mcs[1].begin(), quadrant_mcs[1].end());
    point.mc.insert(point.mc.end(), quadrant_mcs[0].begin(), quadrant_mcs[0].end());
}
//-------------------------------------------------------------------//
void PolygonSkeletonTree::GenerateStats() {
    for (auto circ_it = fCircles.cbegin(); circ_it != fCircles.cend(); ++circ_it) {
        auto parent_pnt_it = fPoints.fPoints.find( circ_it->second.parent_idx );
        if (parent_pnt_it == fPoints.fPoints.cend()){
            // Parent point wasn't found...
            // At present this should never happen
            continue;
        }
        auto parent_stat_it = fStats.find( parent_pnt_it->second.feature );
        if (parent_stat_it == fStats.end()) {
            // This is the first entry
            PolygonSTStats poly_stat;
            poly_stat.max_mc_idx = circ_it->first;
            fStats.emplace(parent_pnt_it->second.feature, poly_stat);
            parent_stat_it = fStats.find( parent_pnt_it->second.feature );
        } else {
            // This is not the first rib for this feature, let's compare the mc radius
            auto parent_circ_it = fCircles.find(parent_stat_it->second.max_mc_idx);
            if (parent_circ_it == fCircles.cend()) {
                // This should never happen, just assign the current circ
                parent_stat_it->second.max_mc_idx = circ_it->first;
            } else {
                if (circ_it->second.radius > parent_circ_it->second.radius ) {
                    parent_stat_it->second.max_mc_idx = circ_it->first;
                }
            }
        }

        auto partic_pnt_it = fPoints.fPoints.find( circ_it->second.partic_idx );
        if (partic_pnt_it == fPoints.fPoints.cend()){
            // Participant point wasn't found...
            // At present this should never happen
            continue;
        }
        auto partic_stat_it = fStats.find( partic_pnt_it->second.feature );
        if (partic_stat_it == fStats.end()) {
            // This is the first entry
            PolygonSTStats poly_stat;
            poly_stat.max_mc_idx = circ_it->first;
            fStats.emplace(partic_pnt_it->second.feature, poly_stat);
            partic_stat_it = fStats.find( partic_pnt_it->second.feature );
        } else {
            // This is not the first rib for this feature, let's compare the mc radius
            auto partic_circ_it = fCircles.find(partic_stat_it->second.max_mc_idx);
            if (partic_circ_it == fCircles.cend()) {
                // This should never happen, just assign the current circ
                partic_stat_it->second.max_mc_idx = circ_it->first;
            } else {
                if (circ_it->second.radius > partic_circ_it->second.radius ) {
                    partic_stat_it->second.max_mc_idx = circ_it->first;
                }
            }
        }

        if (parent_pnt_it->second.feature != partic_pnt_it->second.feature) {
            // This medial circle is connecting two features we want to add connected_features
            // only if this mc is from 2 different features
            auto parent_it = std::find (parent_stat_it->second.connected_features.begin(),
                parent_stat_it->second.connected_features.end(), partic_pnt_it->second.feature);
            if (parent_it == parent_stat_it->second.connected_features.end()){
                parent_stat_it->second.connected_features.push_back(partic_pnt_it->second.feature);
            }
            auto partic_it = std::find (partic_stat_it->second.connected_features.begin(),
                partic_stat_it->second.connected_features.end(), parent_pnt_it->second.feature);
            if (partic_it == partic_stat_it->second.connected_features.end()){
                partic_stat_it->second.connected_features.push_back(parent_pnt_it->second.feature);
            }
             
        }
    }

    // Let's print some debug info
    for (auto stats_it = fStats.cbegin(); stats_it != fStats.cend(); ++stats_it) {
        std::cout << "...Feature " << stats_it->first << " Max MC " << stats_it->second.max_mc_idx;
        auto max_mc_it = fCircles.find(stats_it->second.max_mc_idx);
        std::cout << " with depth " << max_mc_it->second.radius;
        std::cout << " Connected to ";
        for (size_t ii = 0; ii < stats_it->second.connected_features.size(); ++ii) {
            std::cout << stats_it->second.connected_features[ii] << " ";
        }
        std::cout << std::endl;
    }
}
//-------------------------------------------------------------------//
} // namespace glancer
