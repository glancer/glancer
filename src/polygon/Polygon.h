#ifndef GLANCER_POLYGON_H_
#define GLANCER_POLYGON_H_

#include <unordered_map>
#include <vector>

#include "PolyLine2D.h"
#include "PolygonVertex.h"
#include "PolygonSegment.h"
#include "PolygonPolyLine.h"


namespace glancer {

class Polygon {
    friend class PolygonBentleyOttmann;
    friend class PolygonSkeletonTree;
  public:
    Polygon();
    ~Polygon();

    /**
     * This function will return index of the segment inserted
     * If there were the same points for at least 1 edge, it will return 0
     * The order of points matter :)
     */
    size_t AddSegment(Point2D &from, Point2D &to, bool foreign = false);
    /**
     * This function will add content of the other polygon into this polygon
     * If foreign is true, it will add it as foreign polygon
     * The offset can be introduced when adding
     *
     */
    void AddPolygon(Polygon& polygon, Vector2D& offset, bool foreign = false);
    /**
     * Returns number of vertices in the polygon
     */
    size_t getNofVertices() const;
    /**
     * Returns number of segments
     */
    size_t getNofSegments() const;
    /**
     * The bounding box min point
     */
    Point2D& getMin();
    /**
     * The bounding box max point
     */
    Point2D& getMax();
    /**
     * Returns vector summ of all segments in polygon if it's [0,0] the chances are good that
     * this is a closed polygon
     */
    Vector2D& getSegSum();
    /**
     * Finds all the polylines inside this polygon and stores segment indexes
     * in polylines. No special assumptions should be made about polygon
     * itself I think...
     */
    void Decompose();
    /**
     * Computes self-intersections. This is 2 stage process: 
     * First the points are inserted for all found self-intersections. And intermediate
     * map is created that maps affected segment indicies as the key to
     * the list of inserted vertex indicies for this segment.
     *
     * And then segments array altered using this intermidiate map keeping the original segment direction
     */
    void SelfIntersect();

    /**
     * Returns the size of polylines
     */
    size_t GetNofPolylines() const;
    /**
     * Exports PolygonPolyLine with a given number as PolyLine
     */
    PolyLine2D ExportPolyLine(size_t polyline_number);
    /**
     * Gives a winding number of a Polyline
     */
    const long GetPolyLineWindingNumber(size_t polyline_number) const;
    /**
     * Gives Foreign number of a polyline
     */
    const bool GetPolyLineForeignKey(size_t polyline_number) const;
  private:
    /**
     * This function will return the index of the vertex if the point is already in vertices map
     * If the point is not in the array it will pick up next available index, add vertex and return the resulting
     * index
     */
    size_t AddVertex(Point2D &p);
    /**
     * Sets isVisited field for all segments to the given number
     */
    void setAllSegmentsVisited(bool status);
    /**
     * Iterates through segments and returns the index to the first(random) unvisited segment.
     */
    size_t GetUnvisitedSegment();
    /**
     * Returns true if the vertex with the given index branching.
     * The vertex is defined as non-branching when it has exactly 2 (1 from and 1 to) segments
     * with the same foreign key connected to it.
     */
    bool isVertexBranching(const size_t& vertindex);
    /**
     * Given indicies of 2 segments adds crossing point vertex to vertices array and returns index to 
     * this inserted vertex. If there were no crossing point returns 0 (NoPoint)
     */
    size_t AddCrossingVertex(const size_t &segindex1, const size_t &segindex2);
    /**
     * Given a segment index and a list of intersection vertices indicies adds resulting segments
     * keeping their foreign key as the key of the parent
     */
    void AddCrossingSegment(const size_t &segindex, const std::vector<size_t> &vertindex);
    /**
     * Calculates winding number for the given point in polygon
     * if foreign_key is false, the winding number is calculated for non-foreign
     * segments, if it's true, the winding number is calculated for foreign segments.
     */
    long PointWindingNumber(const Point2D& point, bool foreign_key = false) const;
    /**
     * Calculates winding number of the segment given a segment index. The calculation
     * is done for the opposite foreign key, i.e. If the segment is not foreign
     * It will calculate winding number using segments of the foreign polygon.
     * The calculation is done for the point in the middle of the segment.
     */
    long SegmentForeignWindingNumber(const size_t &segindex) const;

  private:
    std::unordered_map <size_t, PolygonSegment> segments;
    size_t nav_segment_idx; //< Stores next available segment index
    std::vector <size_t> avail_segment_idx; // List of deleted segment indexes, that are available.

    // This is a crude way of storing both reverse and forward maps for vertices
    // Though it doubles the amount of memory, but allows fast forward and reverse access.
    std::unordered_map <size_t, PolygonVertex> vertices;
    /**
     * Warning vertices_reverse is used only in AddVertex function to check if the given vertex was added
     * or not. So it should never be used anywhere outside... There will be a freezing of the polygon
     * implemented later on which will clear and regenerate this array if no point additions would be made to the polygon.
     */
    std::unordered_map <Point2D, size_t, Point2D_hash>  vertices_reverse;
    size_t nav_vert_idx;
    std::vector <size_t> avail_vert_idx;
    /**
     * Polylines store the ordered indices of segments of this polygon features.
     * They can be classified later on and exported separately. Like for convex
     * hull one would want only outside border (CCW orientation) but not holes (CW orientation) etc.
     * Also after polygon shrinking or expanding one might want to keep info about
     * double-pass areas (winding number > 1) and treat them differently, or depending on their
     * areas do smaller shrinking.
     */
    std::vector<PolygonPolyLine> polylines;
    /**
     * Bounding box recalculated every time the vertex is added
     */
    Point2D min;
    Point2D max;
    /**
     * This keeps a vector summ of all segments. If zero, there are a good chances this polygon is closed
     */
    Vector2D seg_summ;
};
} // namespace glancer
#endif // GLANCER_POLYGON_H_
