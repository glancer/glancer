#include "Polygon.h"
#include "PolygonBentleyOttmann.h"

#include <algorithm>
#include <iostream>
#include <map>

namespace glancer {
//===================================================================//
PolygonBentleyOttmannSegment::PolygonBentleyOttmannSegment(){
}
//-------------------------------------------------------------------//
PolygonBentleyOttmannSegment::PolygonBentleyOttmannSegment(long _y, size_t _above, size_t _below) :
    y(_y), above(_above), below(_below) {
}
//===================================================================//
PolygonBentleyOttmannEvent::PolygonBentleyOttmannEvent(Point2D& _p, size_t seg_index1, size_t seg_index2) :
    p(_p), seg_index{seg_index1, seg_index2} {
}
//===================================================================//
PolygonBentleyOttmann::PolygonBentleyOttmann():
    fNofIntersectionPoints(0), fNofIntersectionChecks(0),fMaxVertexMultiplicity(2) {
}
//-------------------------------------------------------------------//
PolygonBentleyOttmann::~PolygonBentleyOttmann(){
    fVertices.clear();
}
//-------------------------------------------------------------------//
void PolygonBentleyOttmann::Run(Polygon &polygon) {
    // Clear out previous results
    fVertices.clear();
    fNofIntersectionChecks = 0;
    fNofIntersectionPoints = 0;
    // Initialize Event queue from polygon
    InitializeEventQueue(polygon);
    // std::cout << "BO: events queue size " << fEventQueue.size() << " Should be 2x " << polygon.segments.size() << std::endl;
    // std::cout << "BO: running with " << fMaxVertexMultiplicity << " max vertex multiplicity" << std::endl;
    while (fEventQueue.size() > 0) {
        size_t above_segment = 0; size_t below_segment = 0;
        if (fEventQueue[0].seg_index[1] == 0) {
            // Segment is entering or leaving the fSweepLine
            size_t this_segment = fEventQueue[0].seg_index[0];
            if (! isSegmentPresent(fEventQueue[0].seg_index[0]) ){
                //===> New segment is coming in (left endpoint)
                AddSegment(this_segment, fEventQueue[0].p.Y());
                above_segment = GetSegmentAbove(this_segment);
                below_segment = GetSegmentBelow(this_segment);
                while (above_segment != 0){
                    Intersect(polygon, this_segment, above_segment);
                    above_segment = GetSegmentAbove(above_segment);
                }
                while (below_segment != 0){
                    Intersect(polygon, this_segment, below_segment);
                    below_segment = GetSegmentBelow(below_segment);
                }
            } else {
                //===> Old segment is leaving  (right endpoint)
                above_segment = GetSegmentAbove(this_segment);
                below_segment = GetSegmentBelow(this_segment);
                RemoveSegment(this_segment);
                while ((above_segment != 0)&&(below_segment != 0)){
                    Intersect(polygon, above_segment, below_segment);
                    above_segment = GetSegmentAbove(above_segment);
                    below_segment = GetSegmentBelow(below_segment);
                }
            }
        } else {
            //===> Intersection event
            size_t segment1 = fEventQueue[0].seg_index[0];
            size_t segment2 = fEventQueue[0].seg_index[1];
            SwapSegments(segment1,segment2);
            // We want the highest segment as segment1
            if (fSegments[segment1].y > fSegments[segment2].y) {
                std::swap(segment1, segment2);
            }
            above_segment = GetSegmentAbove(segment1);
            for (short ii = 0; ii < fMaxVertexMultiplicity; ++ii){
                if (above_segment == 0)
                    break;
                Intersect(polygon, segment1, above_segment);
                above_segment = GetSegmentAbove(above_segment);
            }
            below_segment = GetSegmentBelow(segment2);
            for (short ii = 0; ii < fMaxVertexMultiplicity; ++ii){
                if ( below_segment == 0)
                    break;
                Intersect(polygon, segment2, below_segment);
                below_segment = GetSegmentBelow(below_segment);
            }
        }
        // Keep fEventQueue running
        fEventQueue.erase(fEventQueue.begin());
        if (fEventBuffer.size() > 0) {
            // We want to insert accumulated events and sort queue
            fEventQueue.insert(fEventQueue.end(), fEventBuffer.begin(), fEventBuffer.end());
            SortEventQueue();
            fEventBuffer.clear();
        }
        // Let's print debug info
        /*
        std::vector <std::pair<long, size_t>> sorted_segments;
        for (auto it = fSegments.begin(); it != fSegments.end(); ++it) {
            sorted_segments.push_back({it->second.y, it->first});
        }
        std::sort(sorted_segments.begin(), sorted_segments.end());
        std::cout << "Segments in scanline @ " << fEventQueue[0].p.X() << "," << fEventQueue[0].p.Y();
        for (auto it = sorted_segments.begin(); it != sorted_segments.end(); ++it){
            std::cout << " " << it->second;
        }
        std::cout << std::endl;
        */

    }
    //===> The End of the EventQueue processing
    // All working arrays should be empty.
    /*
    std::cout << "Working arrays sizes: fEventQueue=" << fEventQueue.size() 
        << ", fEventBuffer=" << fEventBuffer.size() << ", fSegments=" << fSegments.size()
        << ", fIntersectionMap=" << fIntersectionMap.size() << std::endl;
    */
    std::cout << "Number of Intersection points: " << fNofIntersectionPoints 
        << " Number of Intersection checks: " << fNofIntersectionChecks << std::endl;
    // Let's print the main result
    for (auto it = fVertices.begin(); it != fVertices.end(); ++it) {
        /*
        std::cout << "... Segment " << it->first << " is crossed at [ ";
        for (auto vert_it = it->second.begin(); vert_it != it->second.end(); ++vert_it){
            std::cout << *vert_it << " ";
        }
        std::cout << "] vertices" << std::endl;
        */
        polygon.AddCrossingSegment(it->first, it->second);
    }
    // We can clear out vertices too now
    fVertices.clear();
}
//-------------------------------------------------------------------//
void PolygonBentleyOttmann::InitializeEventQueue(Polygon &polygon){
    fMaxVertexMultiplicity = 1;
    for (auto it = polygon.segments.cbegin(); it != polygon.segments.cend(); ++it) {
        // segment index is stored in it->first
        // vertex index will be in it->second.vertex_index[0] and it->second.vertex_index[1];

        auto vert1_it = polygon.vertices.find(it->second.vertex_index[0]);
        if (vert1_it == polygon.vertices.end())
            return;
        fMaxVertexMultiplicity = std::max(fMaxVertexMultiplicity, 
            vert1_it->second.from_segment_idx.size() + vert1_it->second.to_segment_idx.size());

        auto vert2_it = polygon.vertices.find(it->second.vertex_index[1]);
        if (vert2_it == polygon.vertices.end())
            return;
        fMaxVertexMultiplicity = std::max(fMaxVertexMultiplicity,
            vert2_it->second.from_segment_idx.size() + vert2_it->second.to_segment_idx.size());

        fEventQueue.emplace_back(PolygonBentleyOttmannEvent(vert1_it->second.p, it->first, 0));
        fEventQueue.emplace_back(PolygonBentleyOttmannEvent(vert2_it->second.p, it->first, 0));
    }
    // Keep it sorted
    SortEventQueue();
}
//-------------------------------------------------------------------//
void PolygonBentleyOttmann::SortEventQueue(){
    // This would sort by X and then Y
    std::sort(fEventQueue.begin(), fEventQueue.end(), [](auto& left, auto& right){
        return left.p.X() == right.p.X() ? 
            ( left.p.Y() == right.p.Y() ?
                    left.seg_index[1] > right.seg_index[1] : // Intersection points should be given priority
                left.p.Y() < right.p.Y() ): // sort by Y second
            left.p.X() < right.p.X(); // sort by X first
    });
}
//-------------------------------------------------------------------//
size_t PolygonBentleyOttmann::GetSegmentAbove(size_t segment){
    auto it = fSegments.find(segment);
    if (it == fSegments.end())
        return 0;
    return it->second.above;
}
//-------------------------------------------------------------------//
size_t PolygonBentleyOttmann::GetSegmentBelow(size_t segment){
    auto it = fSegments.find(segment);
    if (it == fSegments.end())
        return 0;
    return it->second.below;
}
//-------------------------------------------------------------------//
bool PolygonBentleyOttmann::isSegmentPresent (size_t segment){
    auto it = fSegments.find(segment);
    if (it == fSegments.end())
        return false;
    return true;
}
//-------------------------------------------------------------------//
void PolygonBentleyOttmann::AddSegment(size_t segment, long y){ 
    size_t best_above_candidate_segment = 0;
    size_t best_below_candidate_segment = 0;
    // for the best robust handling of Y duplicates the easiest way would be to
    // have a sorted pairs array
    std::vector <std::pair<long, size_t>> sorted_segments;
    for (auto it = fSegments.begin(); it != fSegments.end(); ++it) {
        sorted_segments.push_back({it->second.y, it->first});
    }
    sorted_segments.push_back({y, segment});
    std::sort(sorted_segments.begin(), sorted_segments.end());
    for (auto it = sorted_segments.begin(); it != sorted_segments.end(); ++it){
        // Here we will need to find the segment itself and it's above and below
        if (it->second == segment) {
            // we found it
            it++;
            if (it != sorted_segments.end() )
                best_above_candidate_segment = it->second;
            break;
        }
        best_below_candidate_segment = it->second;
    }
    if (best_above_candidate_segment != 0) {
        fSegments[best_above_candidate_segment].below = segment;
    }
    if (best_below_candidate_segment != 0){
        fSegments[best_below_candidate_segment].above = segment;
    }
    fSegments.emplace(segment, PolygonBentleyOttmannSegment(y, best_above_candidate_segment, best_below_candidate_segment));
}
//-------------------------------------------------------------------//
void PolygonBentleyOttmann::RemoveSegment(size_t segment){
    // Let's get below for above and above for below right first
    auto it = fSegments.find(segment);
    if (it == fSegments.end()) {
        std::cout << "Warning was told to remove segment " << segment 
            << " which doesn't exist" << std::endl;
        return;
    }
    if (it->second.above != 0) {
        fSegments[it->second.above].below = it->second.below;
    }
    if (it->second.below != 0) {
        fSegments[it->second.below].above = it->second.above;
    }
    fSegments.erase(it);

    // Remove it from fIntersectionMap too
    auto inters_it = fIntersectionMap.find(segment);
    if (inters_it != fIntersectionMap.end()){
        fIntersectionMap.erase(inters_it);
    }
}
//-------------------------------------------------------------------//
void PolygonBentleyOttmann::SwapSegments(size_t segment1, size_t segment2){
    auto seg1_it = fSegments.find(segment1);
    if (seg1_it == fSegments.end()) {
        std::cout << "Warning: can't swap non-existant (1) " << segment1 << " with " << segment2 << std::endl;
        return;
    }
    auto seg2_it = fSegments.find(segment1);
    if (seg2_it == fSegments.end()) {
        std::cout << "Warning: can't swap non-existant (2) " << segment2 << " with " << segment1 << std::endl;
        return;
    }
    // Swapping is actually done through rebuilding the fSegments via vector array
    std::vector <std::pair<long, size_t>> sorted_segments;
    long segment1_y = fSegments[segment1].y;
    long segment2_y = fSegments[segment2].y;
    fSegments[segment1].y = segment2_y;
    fSegments[segment2].y = segment1_y;
    for (auto it = fSegments.begin(); it != fSegments.end(); ++it) {
        sorted_segments.push_back({it->second.y, it->first});
    }
    std::sort(sorted_segments.begin(), sorted_segments.end());
    for (size_t ii = 0; ii < sorted_segments.size(); ++ii) {
        if (ii == 0)
            fSegments[sorted_segments[ii].second].below = 0;
        else
            fSegments[sorted_segments[ii].second].below = sorted_segments[ii-1].second;

        if (ii == sorted_segments.size()-1)
            fSegments[sorted_segments[ii].second].above = 0;
        else
            fSegments[sorted_segments[ii].second].above = sorted_segments[ii+1].second;
    }
}
//-------------------------------------------------------------------//
void PolygonBentleyOttmann::Intersect(Polygon &polygon, size_t segment1, size_t segment2) {
    // std::cout << "...Intersecting " << segment1 << " with " << segment2 << std::endl;
    auto seg1_im_iter = fIntersectionMap.find(segment1);
    if (seg1_im_iter == fIntersectionMap.end()) {
        // Intersection wasn't attempted for this segment at all, so let's create it
        seg1_im_iter = fIntersectionMap.emplace(segment1,std::vector<size_t>{}).first;
    }
    // At this point seg1_im_iter should be valid
    auto seg2_im_iter = fIntersectionMap.find(segment2);
    if (seg2_im_iter == fIntersectionMap.end()) {
        // Intersection wasn't attempted for this segment at all, so let's create it
        seg2_im_iter = fIntersectionMap.emplace(segment2,std::vector<size_t>{}).first;
    }
    // At this point both seg1_im_iter and seg2_im_iter should be valid
    // Let's first check the list for segment1 and if segment2 is found there, bail out
    auto search_it = std::find(seg1_im_iter->second.begin(), seg1_im_iter->second.end(), segment2);
    if (search_it != seg1_im_iter->second.end() ) {
        // std::cout << "Segment " << *search_it << " is found in "  << seg1_im_iter->first << " Intersection list" << std::endl;
        // soo bailing out at this point
        return;
    } else {
        // std::cout << "Segment " << segment2 << " is not found in " << seg1_im_iter->first << " Intersection list" << std::endl;
        // So push it back
        seg1_im_iter->second.push_back(segment2);
    }
    // Segment1 can intersect with Segment2 but not vice versa in case of parallel overlapping segments
    search_it = std::find(seg2_im_iter->second.begin(), seg2_im_iter->second.end(), segment1);
    if (search_it != seg2_im_iter->second.end() ) {
        // std::cout << "Segment " << *search_it << " is found in "  << seg2_im_iter->first << " Intersection list" << std::endl;
        // This should never happen...
        return;
    } else {
        // std::cout << "Segment " << segment1 << " is not found in " << seg2_im_iter->first << " Intersection list" << std::endl;
        seg2_im_iter->second.push_back(segment1);
    }
    // At this point we didn't service this particular segments combination , so intersect
    size_t crossing_vertex_index1 = polygon.AddCrossingVertex(segment1, segment2);
    fNofIntersectionChecks++;
    size_t crossing_vertex_index2 = polygon.AddCrossingVertex(segment2, segment1);
    fNofIntersectionChecks++;

    if ((crossing_vertex_index1 == 0)&&(crossing_vertex_index2 == 0)) {
        // There were no crossing, so we are essentially done?
        return;
    }
    if (crossing_vertex_index1 == crossing_vertex_index2) {
        std::cout << " BO says segments " << segment1 << " and " << segment2 << " are crossing at vertex " << crossing_vertex_index1 << std::endl;
        fEventBuffer.emplace_back(polygon.vertices[crossing_vertex_index1].p, segment1, segment2);
        fVertices[segment1].push_back(crossing_vertex_index1);
        fVertices[segment2].push_back(crossing_vertex_index2);
    } else if (crossing_vertex_index1 != 0) {
        // segment1 is crossing but not segment2
        std::cout << " BO says segment " << segment1 << " is crossing at " << crossing_vertex_index1 << " but not" << segment2 << std::endl;
        fEventBuffer.emplace_back(polygon.vertices[crossing_vertex_index1].p, segment1, segment2);
        fVertices[segment1].push_back(crossing_vertex_index1);
    } else {
        // segment2 is crossing, but not segment1
        std::cout << " BO says segment " << segment2 << " is crossing at " << crossing_vertex_index2 << " but not" << segment1 << std::endl;
        fEventBuffer.emplace_back(polygon.vertices[crossing_vertex_index2].p, segment1, segment2);
        fVertices[segment2].push_back(crossing_vertex_index2);
    }
    fNofIntersectionPoints++;
}
//-------------------------------------------------------------------//
} // namespace glancer
