#ifndef POLYGONSKELETONTREE_H_
#define POLYGONSKELETONTREE_H_

#include <unordered_map>
#include <vector>

#include "SurfacePlane.h"
#include "Vector3D.h"
#include "Point3D.h"
#include "Polygon.h"

#include "external/nanoflann.hpp"
namespace glancer {

/**
 * Skeleton Tree Point class which holds  3D vector
 * With some additional info about neighbouring bissectors, connected via the edge
 */
class PolygonSTPoint{
  public:
    // Polygon ordered indicies
    size_t      prev;       // Index of the previous ST point in the polygon
    size_t      next;       // Index of the next ST point in the polygon
    size_t      feature;    // The feature inside polygon this point belongs to
    // The point data
    Point2D     p;          // The point itself
    Vector2D    b;          // Bisector vector
    // Servicing flags
    bool    isVisited;
    // Connection list to medial circles
    std::vector <size_t>    mc;
};
/**
 * Class for handling ST Points cloud.
 */
class PolygonSTPointCloud{
    friend class SvgExporter;
  public:
    PolygonSTPointCloud();
    ~PolygonSTPointCloud();
    /**
     * Goes through all the points and sets visited flag to a specified value
     */
    void SetAllPointsVisited(bool visited_flag);
    /**
     * Gets (random) unvisited point index
     */
    size_t GetRandomUnvisitedPointIndex();
    /**
     * Adds point to the point cloud
     */
    void Add(size_t index, const PolygonSTPoint& point);
    /**
     * Builds KDTree index this should be executed after all points were added
     */
    void BuildIndex();
    /**
     * Returns index of the ST point, closest to the sample point,
     * excluding the point with given index
     */
    size_t GetClosestPointIndexExcluding(const Point2D& sample, size_t excluding);
    /**
     * Mandatory functions used by nanoflann
     */
    // Must return the number of data points
	inline size_t kdtree_get_point_count() const { return fPointsMap.size(); }
    // Returns the dim'th component of the idx'th point in the class
    inline long kdtree_get_pt(const size_t idx, const size_t dim) const {
        auto point_it = fPoints.find(fPointsMap[idx]);
        // This should never happen, but just in case let's put a guard here for now
        if (point_it == fPoints.cend())
            return 0;
        if (dim == 0) 
            return point_it->second.p.X();
        else 
            return point_it->second.p.Y();
    }
    // Optional bounding-box computation: return false to default to a standard bbox computation loop.
    template <class BBOX>
	    bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }
  public:
    std::unordered_map <size_t, PolygonSTPoint>  fPoints; // Polygon ST points
    std::vector <size_t> fPointsMap; // This maps values from unordered map indicies to continious numbering used by nanoflann
    size_t fNavPointIndex; // Stores the next available point index
    nanoflann::KDTreeSingleIndexAdaptor< nanoflann::L2_Simple_Adaptor<long, PolygonSTPointCloud > ,
        PolygonSTPointCloud, 2 /* dim */ > fKDTree;
};
/**
 *  Skeleton Tree Medial circle class.
 */
class PolygonSTMedialCircle {
  public:
    Point2D center;
    long    radius;
    size_t  parent_idx; // Index of the parent point
    size_t  partic_idx; // Index of the participant point
    // Servicing flags
    bool    isVisited;
};
/**
 * Skeleton Tree Statistics class for a given feature.
 */
class PolygonSTStats {
  public:
    size_t  max_mc_idx; // Index of the medial circle with max radius for this feature
    std::vector <size_t> connected_features; // List of feature indicies, connected through the same medial circle
};
/**
 * Polygon SkeletonTree is a 3Dimensional structure created from 2D polygon
 * Which basically tells you how deep inside the polygon you are at a given 2D coordinates.
 */
class PolygonSkeletonTree {
    friend class SvgExporter;
  public:
    PolygonSkeletonTree(long max_z_distance, long resample_distance);
    ~PolygonSkeletonTree();

    /**
     * This will load data from Polygon features, The polygon will need to be decomposed.
     */
    void LoadBaseFromDecomposedPolygon(const Polygon& polygon);

    /**
     * This will run iterative process of building the skeleton
     */
    void Construct();
    /**
     * Returns number of features
     */
    const size_t GetNofFeatures() const;

  private:
    /**
     * Goes through all medial circles and adds connection to points
     */
    void AddMedialCirclesConnectionToPoints();
    /**
     * Sorts medial connections to a certain point
     */
    void SortMedialCirclesConnectionToPoints();
    /**
     * Starting with given medial circle reduce it's radius and move the center
     * along the parent point bisector line, until it fits the polygon. 
     * Returns the number of iteration steps.
     */
    size_t FitMedialCircleToPolygon(PolygonSTMedialCircle& circle);
    /**
     * Given 2 bisectors, returns true and the point if there was an intersection
     * returns false if there were no intersection.
     */
    bool FindIntersectionPoint(const Point2D& pointA, const Point2D& pointB, 
        const Point2D& pointC, const Point2D& pointD, Point2D &result) const;
    /**
     * Given 3 points, returns bisector vector for the middle one.
     */
    Vector2D GetBisectorVector(const Point2D& left, const Point2D& middle, const Point2D& right);
    /**
     * Given 2 points for line segment and previously accumulated event length
     * Inserts points into fPoints array evenly distributed with fResampleDist apart
     */
    void InsertSamplePoints(const Point2D& from_pnt, const Point2D& to_pnt, long &accum_length, size_t feature);
    /**
     * Goes through all of the points and recompute bisector vectors
     */
    void RecomputeBisectorVectors();
    /**
     * For each point generates a medial axis circle
     */
    void GenerateMedialAxisCircles();
    /**
     *  Given a point sorts Connections from this point to medial circles
     */
    void SortMedialCirclesConnectionToPoint(PolygonSTPoint& point);
    /**
     * Generate statistics
     */
    void GenerateStats();

  private:
    PolygonSTPointCloud  fPoints; // Polygon points cloud

    std::unordered_map <size_t, PolygonSTMedialCircle> fCircles;
    size_t fNavCircle; // Stores the index for next available medial circle

    // Statistics per each feature, like Min/Max ribs connection points etc
    std::unordered_map <size_t, PolygonSTStats> fStats;

    size_t  fNofFeatures; // Number of features in this skeleton tree
    long fMaxZ;  // Maximum depth to which build polygon tree       
    long fResampleDist; // Resample length distance.
};
} // namespace glancer
#endif // POLYGONSKELETONTREE_H_
