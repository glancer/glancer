#include "PolygonSegment.h"

namespace glancer {
//-------------------------------------------------------------------//
PolygonSegment::PolygonSegment():
        vertex_index{0,0},
        isVisited(false), isForeign(false) {
}
//-------------------------------------------------------------------//
PolygonSegment::PolygonSegment(std::size_t from, std::size_t to, bool foreign) :
        vertex_index {from,to},
        isVisited(false), isForeign(foreign) {
}
//-------------------------------------------------------------------//
PolygonSegment::~PolygonSegment(){
}
//-------------------------------------------------------------------//
} // namespace glancer
