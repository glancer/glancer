#ifndef GLANCER_POLYGONPOLYLINE_H_
#define GLANCER_POLYGONPOLYLINE_H_

#include <vector>

namespace glancer {

class PolygonPolyLine {
  public:
    PolygonPolyLine();
    ~PolygonPolyLine();
  public:
    std::size_t from_vertex_index;
    std::size_t to_vertex_index;
    std::vector <std::size_t> segment_indexes;
    bool isForeign;
    long winding_number;
};
} // namespace glancer
#endif // GLANCER_POLYGONPOLYLINE_H_
