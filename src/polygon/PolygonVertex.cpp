#include "PolygonVertex.h"

#include <algorithm> // for std::find

namespace glancer {
//-------------------------------------------------------------------//
PolygonVertex::PolygonVertex() :
        p{0,0} {
}
//-------------------------------------------------------------------//
PolygonVertex::PolygonVertex(const Point2D& _p) :
        p(_p) {
}
//-------------------------------------------------------------------//
PolygonVertex::~PolygonVertex(){
}
//-------------------------------------------------------------------//
void PolygonVertex::AddFrom(const size_t &segment_idx){
    auto it = std::find(from_segment_idx.begin(), from_segment_idx.end(), segment_idx);
    if (it == from_segment_idx.end())
        from_segment_idx.push_back(segment_idx);
}
//-------------------------------------------------------------------//
void PolygonVertex::AddTo(const size_t &segment_idx){
    auto it = std::find(to_segment_idx.begin(), to_segment_idx.end(), segment_idx);
    if (it == to_segment_idx.end())
        to_segment_idx.push_back(segment_idx);
}
//-------------------------------------------------------------------//
void PolygonVertex::RemoveFrom(const size_t &segment_idx){
    auto it = std::find(from_segment_idx.begin(), from_segment_idx.end(), segment_idx);
    if (it != from_segment_idx.end())
        from_segment_idx.erase(it);
}
//-------------------------------------------------------------------//
void PolygonVertex::RemoveTo(const size_t &segment_idx){
    auto it = std::find(to_segment_idx.begin(), to_segment_idx.end(), segment_idx);
    if (it != to_segment_idx.end())
        to_segment_idx.erase(it);
}
//-------------------------------------------------------------------//
} // namespace glancer
