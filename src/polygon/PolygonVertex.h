#ifndef GLANCER_POLYGONVERTEX_H_
#define GLANCER_POLYGONVERTEX_H_

#include "Point2D.h"
#include <cstddef> // for size_t
#include <vector>

namespace glancer {
/**
 * This class is essentially Point2D
 * But with additional connectivity info for Polygon
 * (like indexes of connected segments etc.)
 * If needed this info will be added here later
 */
class PolygonVertex {
  public:
    // Apparently unordered maps do want a public constructor.
    PolygonVertex();
    PolygonVertex(const Point2D& _p);
    ~PolygonVertex();

    /**
     * Adds segment index if it wasn't present in from_segment_idx
     * otherwise does nothing
     */
    void AddFrom(const size_t &segment_idx);
    /**
     * Adds segment index if it wasn't present in to_segment_idx
     * otherwise does nothing
     */
    void AddTo(const size_t &segment_idx);
    /**
     * Removes segment index if it was present in from_segment_idx
     * otherwise does nothing
     */
    void RemoveFrom(const size_t &segment_idx);
    /**
     * Removes segment index if it was present in to_segment_idx
     * otherwise does nothing
     */
    void RemoveTo(const size_t &segment_idx);

  public:
    Point2D p;
    // Each vertex can have more than one segment coming to and from it
    std::vector<size_t> from_segment_idx; // stores indexes of segments coming into this vertex
    std::vector<size_t>   to_segment_idx; // stores indexes of segments coming out of this vertex
};
/**
 * This is used as a hash function in reverse vertex maps
 */
struct Point2D_hash {
    size_t operator() (const Point2D &p) const {
        static int prime = 31;
        int result = 89;
        result = result * prime + p.X();
        result = result * prime + p.Y();
        return result;
        // Just fo reference here for 3D point we used this:
        // return p.X() ^ (p.Y() << 10) ^ (p.Z() << 20);
    }
};
} // namespace glancer
#endif // GLANCER_POLYGONVERTEX_H_
