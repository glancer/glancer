#ifndef GLANCER_POINT2D_H_
#define GLANCER_POINT2D_H_

namespace glancer {
    class Point3D;

class Point2D {
    friend class PolygonVertex;
    friend class Polygon;
    friend class Vector2D;
  public:
    Point2D(long _x, long _y);
    /**
     * It's possible to reduce the dimension but not the other way around
     */
    Point2D(const Point3D& p);
    ~Point2D();

    /**
     * For accessing point coordinates directly
     */
    const long X() const;
    const long Y() const;
    /**
     * Operators
     */
    bool operator ==(const Point2D& p) const;
    Point2D& operator = (const Point2D& p);

  private:
    long x,y;
};
} // namespace glancer
#endif // GLANCER_POINT2D_H_
