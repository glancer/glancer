#include <iostream> // for cout
#include <sstream>  // for string conversion

#include "Mesh.h"
#include "MeshFileLoader.h"
#include "SurfacePlane.h"
#include "Polygon.h"
#include "PolygonSkeletonTree.h"
#include "SvgExporter.h"

int main(int argc, char **argv){
using namespace std;
using namespace glancer;

long z_ofset = 0; 
bool z_offset_is_valid = false;

    vector<string> fileNames;
    for(size_t argn = 1; argn < argc; argn++) {
        std::string arg = argv[argn];
        if (arg == "-i") {
            if (argn + 1 < argc) {
                // make sure the argument is passed
                argn++;
                fileNames.push_back(string(argv[argn]));
            }
        } else if (arg == "-s") {
            if (argn + 1 < argc){
                // make sure the argument is passed
                argn++;
                std::istringstream str_stream(argv[argn] );
                if ((str_stream >> z_ofset) && str_stream.eof()) {
                    // Conversion successful
                    z_offset_is_valid = true;
                }
            }
        }
    }

    if (!z_offset_is_valid) {
        cout << "Provide slicing offset with -s option" << endl;
        return 0;
    }

    if (fileNames.size() <= 0) {
        cout << "Provide at least one filename with -i option" << endl;
        return 0;
    }
    Mesh mesh;
    MeshFileLoader file(1000); // mm to int conversion is 1000 or 1micron resolution

    cout << "...Testing mesh loading " << endl;
    if ( !file.LoadBinarySTL(fileNames[0], mesh ) ) {
        cout << "   Failed to load mesh from \"" << fileNames[0] << "\"" << endl;
        return -1;
    } else {
        cout << "   Mesh is loaded from \"" << fileNames[0] << "\"" << endl;
        cout << "       NofVertices=" << mesh.getNofVertices() << "  NofFaces=" << mesh.getNofFaces() << endl;
        cout << "       Bounding Box : [" << mesh.getMin().X() << ";" << mesh.getMin().Y() << ";" << mesh.getMin().Z() 
                    << "]  [" << mesh.getMax().X() << ";" << mesh.getMax().Y() << ";" << mesh.getMax().Z() << "]" << endl;
    }

    cout << "...Testing plane creation" << endl;
    // We create a plane that is centered at our dataset in X and Y, and 100 microns above Z min
    
    Point3D A ((mesh.getMin().X()+mesh.getMax().X())/2, (mesh.getMin().Y()+mesh.getMax().Y())/2, mesh.getMin().Z() + z_ofset);
    Point3D B (                      mesh.getMax().X(), (mesh.getMin().Y()+mesh.getMax().Y())/2, mesh.getMin().Z() + z_ofset);
    Point3D C (                      mesh.getMax().X(),                       mesh.getMax().Y(), mesh.getMin().Z() + z_ofset);
    
    /*
    Point3D A ( mesh.getMin().X(),  (mesh.getMin().Y()+mesh.getMax().Y())/2,    mesh.getMin().Z() );
    Point3D B ( mesh.getMax().X(),  (mesh.getMin().Y()+mesh.getMax().Y())/2,    mesh.getMin().Z() );
    Point3D C ( mesh.getMax().X(),  (mesh.getMin().Y()+mesh.getMax().Y())/2,    mesh.getMax().Z() );
    */
    SurfacePlane surface(A, B, C );
    
    cout << "...Testing Mesh slicer" << endl;
    Polygon polygon = mesh.SliceWithPlane(surface);
    cout << "   Mesh is sliced " << endl;
    cout << "       NofVertices=" << polygon.getNofVertices() << "  NofSegments=" << polygon.getNofSegments() << endl;
    cout << "       Bounding Box : [" << polygon.getMin().X() << ";" << polygon.getMin().Y()
            << "]  [" << polygon.getMax().X() << ";" << polygon.getMax().Y() << "]" << endl;
    cout << "       Segments Summ : [" << polygon.getSegSum().X() << ";" << polygon.getSegSum().Y() << "]" << endl;

    cout << "...Testing decomposition" << endl;
    polygon.Decompose();
    cout << "...Testing Skeleton Tree Creation" << endl;
    PolygonSkeletonTree st_poly(20000, 200); // For now let's make max dimensions small
    st_poly.LoadBaseFromDecomposedPolygon(polygon);
    st_poly.Construct();

    cout << "...Testing exporting to SVG" << endl;
    SvgExporter svg_file(polygon.getMin(), polygon.getMax(), 0.01);
    svg_file.Open("glancer_test_offset.svg");
    SvgExporter::Color polyline_color(0,0,0);

    // Drawing the original polygon
    for (size_t ii = 0; ii < polygon.GetNofPolylines(); ++ii) {    
        PolyLine2D polyline_feature = polygon.ExportPolyLine(ii);
        svg_file.WritePolyLine(polyline_feature, polyline_color);
    }
    // Drawing the Skeleton Tree
    svg_file.WritePolygonSkeletonTree(st_poly);

    svg_file.Close();
    cout << "...End of the test" << endl;
    return 0;
}
