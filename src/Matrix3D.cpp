#include "Matrix3D.h"

#include <cstring> // For memcpy

namespace glancer {
//-------------------------------------------------------------------//
Matrix3D::Matrix3D() {
}
//-------------------------------------------------------------------//
Matrix3D::Matrix3D(const Matrix3D& matrix){
    memcpy(m, matrix.m, 9*sizeof(float));
}
//-------------------------------------------------------------------//
Matrix3D::~Matrix3D(){
}
//-------------------------------------------------------------------//
float* Matrix3D::operator[] (size_t row) const{
    return const_cast<float*>(m[row]);
}
//-------------------------------------------------------------------//
} // namespace glancer
