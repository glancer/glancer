#ifndef GLANCER_MATRIX3D_H_
#define GLANCER_MATRIX3D_H_

#include <cstddef>  // for size_t

namespace glancer {

class Matrix3D {
  public:
    /**
     * Default constructor is not initializing the matrix
     */
    Matrix3D();
    /**
     * Copy constructor from other matrix
     */
    Matrix3D(const Matrix3D& matrix);
    ~Matrix3D();

    /**
     * Access matrix elements with matrix[row][column]
     */
    float* operator[] (size_t row) const;

  private:
    float m[3][3];
};
} // namespace glancer
#endif // GLANCER_MATRIX3D_H_
