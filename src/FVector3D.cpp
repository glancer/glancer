#include <cmath> // for norm()

#include "FVector3D.h"
#include "Vector3D.h"

namespace glancer {
//-------------------------------------------------------------------//
FVector3D::FVector3D(float _x, float _y, float _z) :
        x(_x), y(_y), z(_z) {
}
//-------------------------------------------------------------------//
FVector3D::FVector3D(const Vector3D &int_vector):
        x(int_vector.X()), y(int_vector.Y()), z(int_vector.Z()) {
}
//-------------------------------------------------------------------//
FVector3D::~FVector3D(){
}
//-------------------------------------------------------------------//
float FVector3D::X() const {
    return x;
}
//-------------------------------------------------------------------//
float FVector3D::Y() const {
    return y;
}
//-------------------------------------------------------------------//
float FVector3D::Z() const {
    return z;
}
//-------------------------------------------------------------------//
FVector3D FVector3D::cross(const FVector3D &vec) const {
    return FVector3D (
        y*vec.z - z*vec.y,
        z*vec.x - x*vec.z,
        x*vec.y - y*vec.x );
}
//-------------------------------------------------------------------//
float FVector3D::dot(const FVector3D &p) const {
    return x*p.x + y*p.y + z*p.z;
}
//-------------------------------------------------------------------//
float FVector3D::norm2() const {
    return x*x + y*y + z*z;
}
//-------------------------------------------------------------------//
float FVector3D::norm() const {
    return std::sqrt(norm2());
}
//-------------------------------------------------------------------//
void FVector3D::normalize() {
    float norm = norm2();
    if ( norm > 0.0 ) {
        norm = 1.0/std::sqrt(norm);
        x *= norm; y *= norm; z *= norm;
    }
}
//-------------------------------------------------------------------//
FVector3D FVector3D::operator + (const FVector3D& vec) const {
    return FVector3D(x + vec.x, y + vec.y, z + vec.z);
}
//-------------------------------------------------------------------//
FVector3D FVector3D::operator * (const float f) const { 
    return FVector3D(x*f, y*f, z*f); 
}
//-------------------------------------------------------------------//
FVector3D& FVector3D::operator *= (const float f) {
    x *= f; y *= f; z *= f; 
    return *this;
}
//-------------------------------------------------------------------//
} // namespace glancer
