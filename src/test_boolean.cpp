#include <iostream> // for cout

#include "Mesh.h"
#include "MeshFileLoader.h"
#include "SurfacePlane.h"
#include "Polygon.h"
#include "SvgExporter.h"

int main(int argc, char **argv){
using namespace std;
using namespace glancer;

    vector<string> fileNames;
    for(size_t argn = 1; argn < argc; argn++) {
        std::string arg = argv[argn];
        if (arg == "-i") {
            if (argn + 1 < argc) {
                // make sure the argument is passed
                argn++;
                fileNames.push_back(string(argv[argn]));
            }
        }
    }

    if (fileNames.size() <= 0) {
        cout << "Provide at least one filename" << endl;
        return 0;
    }
    Mesh mesh;
    MeshFileLoader file(1000); // mm to int conversion is 1000 or 1micron resolution

    cout << "...Testing mesh loading " << endl;
    if ( !file.LoadBinarySTL(fileNames[0], mesh ) ) {
        cout << "   Failed to load mesh from \"" << fileNames[0] << "\"" << endl;
        return -1;
    } else {
        cout << "   Mesh is loaded from \"" << fileNames[0] << "\"" << endl;
        cout << "       NofVertices=" << mesh.getNofVertices() << "  NofFaces=" << mesh.getNofFaces() << endl;
        cout << "       Bounding Box : [" << mesh.getMin().X() << ";" << mesh.getMin().Y() << ";" << mesh.getMin().Z() 
                    << "]  [" << mesh.getMax().X() << ";" << mesh.getMax().Y() << ";" << mesh.getMax().Z() << "]" << endl;
    }
    cout << "...Testing plane creation" << endl;
    // We create a plane that is centered at our dataset in X and Y, and 100 microns above Z min
    long z_ofset = 100; // 
    Point3D A ((mesh.getMin().X()+mesh.getMax().X())/2, (mesh.getMin().Y()+mesh.getMax().Y())/2, mesh.getMin().Z() + z_ofset);
    Point3D B (                      mesh.getMax().X(), (mesh.getMin().Y()+mesh.getMax().Y())/2, mesh.getMin().Z() + z_ofset);
    Point3D C (                      mesh.getMax().X(),                       mesh.getMax().Y(), mesh.getMin().Z() + z_ofset);
    SurfacePlane surface(A, B, C );
    Point3D A_plane = surface.ToSurface(A);
    Point3D B_plane = surface.ToSurface(B);
    Point3D C_plane = surface.ToSurface(C);
    cout << "   Plane coordinates of points:" 
        << "A=[" << A_plane.X() << ";" << A_plane.Y() << ";" << A_plane.Z() << "] "
        << "B=[" << B_plane.X() << ";" << B_plane.Y() << ";" << B_plane.Z() << "] "
        << "C=[" << C_plane.X() << ";" << C_plane.Y() << ";" << C_plane.Z() << "] "
        << endl;
    
    cout << "...Testing Mesh slicer" << endl;
    Polygon polygon = mesh.SliceWithPlane(surface);
    cout << "   Mesh is sliced " << endl;
    cout << "       NofVertices=" << polygon.getNofVertices() << "  NofSegments=" << polygon.getNofSegments() << endl;
    cout << "       Bounding Box : [" << polygon.getMin().X() << ";" << polygon.getMin().Y()
            << "]  [" << polygon.getMax().X() << ";" << polygon.getMax().Y() << "]" << endl;
    cout << "       Segments Summ : [" << polygon.getSegSum().X() << ";" << polygon.getSegSum().Y() << "]" << endl;

    cout << "...Testing Self intersection" << endl;
    Point2D A2D = A_plane;
    Point2D B2D = B_plane;
    Point2D C2D = C_plane;
    // Adding plane polygon as foreign
    polygon.AddSegment(A2D,B2D,true);
    polygon.AddSegment(B2D,C2D,true);
    polygon.AddSegment(C2D,A2D,true);
    polygon.SelfIntersect();

    cout << "...Testing Feature finding inside Polygon" << endl;
    polygon.Decompose();

    cout << "...Testing exporting to SVG" << endl;
    SvgExporter svg_file(polygon.getMin(), polygon.getMax(), 0.02);
    svg_file.Open("glancer_test_boolean.svg");
    SvgExporter::Color polyline_color(0,0,0);
    for (size_t ii = 0; ii < polygon.GetNofPolylines(); ++ii) {
        
        /** Rainbow
        int sample_size = polygon.GetNofPolylines();
        int r = ii * 255 / sample_size;
        int g = (ii * 255 * 11 / sample_size) % (255 * 2);
        int b = (ii * 255 * 5 / sample_size) % (255 * 2);
        if (b > 255) 
            b = 255 * 2 - b;
        polyline_color.r = r;
        polyline_color.g = g;
        polyline_color.b = b;
        */
        polyline_color.r = 0;
        polyline_color.g = 0;
        polyline_color.b = 0; 
        if ( polygon.GetPolyLineForeignKey(ii) ) {
            // If this is a foreign polyline, give it red tint
            if ( polygon.GetPolyLineWindingNumber(ii) > 0 ) {
                polyline_color.g = 255;
                polyline_color.b = 255;
            } else {
                polyline_color.r = 255;
            }
        } else {
            // This is a native polygon
            if ( polygon.GetPolyLineWindingNumber(ii) > 0 ) {
                polyline_color.b = 255;
            }
        }


        PolyLine2D polyline_feature = polygon.ExportPolyLine(ii);
        svg_file.WritePolyLine(polyline_feature, polyline_color);
        /*
        cout << "Exported polyline with VectorSumm=[" << polyline_feature.GetVectorSumm().X() << "," 
            << polyline_feature.GetVectorSumm().Y() << "] , size="
            << polyline_feature.GetVectors().size()
            << endl;
        */
    }
    svg_file.Close();
    cout << "...End of the test" << endl;
    return 0;
}
