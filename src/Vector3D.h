#ifndef GLANCER_VECTOR3D_H_
#define GLANCER_VECTOR3D_H_

#include "Point3D.h"

namespace glancer {

class Vector3D {
  public:
    Vector3D(long _x, long _y, long _z);
    Vector3D(const Point3D &from, const Point3D &to);
    ~Vector3D();
    /**
     * For accessing the vector coordinates directly
     */
    long X() const; 
    long Y() const; 
    long Z() const;
    /**
     * Cross product of this vector and another vector
     */
    Vector3D cross(const Vector3D &vec) const;
    /**
     * Dot product of this vector and another vector
     */
    long dot(const Vector3D &p) const;
    /**
     * Returns the norm^2 of the vector
     */
    long norm2();
    /**
     * Returns the norm of the vector
     */
    float norm();

    /**
     * Multiplies this vector by a scalar
     */
    Vector3D& operator *= (const long s);
    /**
     * Multiplies this vector by a float scalar
     */
    Vector3D& operator *= (const float s);
    /**
     * Divides this vector by a scalar
     */
    Vector3D& operator /= (const long s);

  private:
    long x,y,z;
};
inline static Vector3D operator+(const Vector3D& vec1, const Vector3D& vec2){
    return Vector3D(vec1.X()+vec2.X(), vec1.Y()+vec2.Y(), vec1.Z()+vec2.Z());
}
} // namespace glancer
#endif // GLANCER_VECTOR3D_H_
