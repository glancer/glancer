#ifndef GLANCER_VECTOR2D_H_
#define GLANCER_VECTOR2D_H_

#include "Point2D.h"

namespace glancer {

class Vector2D {
    friend class Polygon;
  public:
    Vector2D(long _x, long _y);
    Vector2D(const Point2D &from, const Point2D &to);
    ~Vector2D();
    /**
     * For accessing the vector coordinates directly
     */
    long X() const; 
    long Y() const; 
    /** 
     * Cross product of this vector and another vector
     */
    long cross(const Vector2D &vec) const; 
    /**
     * Dot product of this vector and another vector
     */
    long dot(const Vector2D &p) const;
    /**
     * Returns the norm^2 of the vector
     */
    const long norm2() const;
    /**
     * Returns the norm of the vector
     */
    const float norm() const;

    /**
     * Multiplies this vector by a scalar
     */
    Vector2D& operator *= (const long s);
    /**
     * Multiplies vector by float scalar
     */
    Vector2D& operator *= (const float s);
    /**
     * Divides this vector by a scalar
     */
    Vector2D& operator /= (const long s);
    /**
     * Adds another vector to this vector
     */
    Vector2D& operator += (const Vector2D& v);
    /**
     * Subtract another vector from this vector
     */
    Vector2D& operator -= (const Vector2D& v);

  private:
    long x,y;
};
    inline static Vector2D operator/(const Vector2D& vec, const long div){
        return Vector2D(vec.X()/div, vec.Y()/div);
    }
    inline static Point2D operator+(const Point2D& point, const Vector2D& vector){
        return Point2D(point.X()+vector.X(), point.Y()+vector.Y());
    }
    inline static Vector2D operator+(const Vector2D& vec1, const Vector2D& vec2){
        return Vector2D(vec1.X()+vec2.X(), vec1.Y()+vec2.Y());
    }
    inline static Vector2D operator*(const long mult, const Vector2D& vec){
        return Vector2D(vec.X()*mult, vec.Y()*mult);
    }
} // namespace glancer
#endif // GLANCER_VECTOR2D_H_
