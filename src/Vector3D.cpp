#include <cmath> // for norm()

#include "Vector3D.h"

namespace glancer {
//-------------------------------------------------------------------//
Vector3D::Vector3D(long _x, long _y, long _z) :
        x(_x), y(_y), z(_z) {
}
//-------------------------------------------------------------------//
Vector3D::Vector3D(const Point3D &from, const Point3D &to):
        x(to.x - from.x), y(to.y - from.y), z(to.z - from.z) {
}
//-------------------------------------------------------------------//
Vector3D::~Vector3D(){
}
//-------------------------------------------------------------------//
long Vector3D::X() const {
    return x;
}
//-------------------------------------------------------------------//
long Vector3D::Y() const {
    return y;
}
//-------------------------------------------------------------------//
long Vector3D::Z() const {
    return z;
}
//-------------------------------------------------------------------//
Vector3D Vector3D::cross(const Vector3D &vec) const {
    return Vector3D (
        y*vec.z - z*vec.y,
        z*vec.x - x*vec.z,
        x*vec.y - y*vec.x );
}
//-------------------------------------------------------------------//
long Vector3D::dot(const Vector3D &p) const {
    return x*p.x + y*p.y + z*p.z;
}
//-------------------------------------------------------------------//
long Vector3D::norm2() {
    return x*x + y*y + z*z;
}
//-------------------------------------------------------------------//
float Vector3D::norm() {
    return std::sqrt(norm2());
}
//-------------------------------------------------------------------//
Vector3D& Vector3D::operator *= (const long s){
    x *= s; y *= s; z *= s;
    return *this;
}
//-------------------------------------------------------------------//
Vector3D& Vector3D::operator *= (const float s){
    x *= s; y *= s; z *= s;
    return *this;
}
//-------------------------------------------------------------------//
Vector3D& Vector3D::operator /= (const long s){
    x /= s; y /= s; z /= s;
    return *this;
}
//-------------------------------------------------------------------//
} // namespace glancer
