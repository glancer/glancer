#ifndef GLANCER_POINT3D_H_
#define GLANCER_POINT3D_H_

namespace glancer {
    class Vector3D;
class Point3D {
    friend class Point2D;
    friend class Vector3D;
    friend class MeshVertex;
    friend class Mesh;
  public:
    Point3D(long _x, long _y, long _z);
    ~Point3D();
    /**
     * For accessing point coordinates directly
     */
    long X() const;
    long Y() const;
    long Z() const;
    /**
     * Operators
     */
    bool operator ==(const Point3D& p) const;
    Point3D& operator = (const Point3D& p);
    Point3D& operator += (const Vector3D& v);
  private:
    /*
     * Uninitialized constructor is for friends who know what they are doing only
     */
    Point3D();

  private:
    long x,y,z;
};

/**
 * Offsets pont by the vector
 */
const Point3D operator+(const Point3D&  p, const Vector3D& v);
const Point3D operator+(const Vector3D& v, const Point3D&  p);

} // namespace glancer
#endif // GLANCER_POINT3D_H_
