#include "PolyLine2D.h"

namespace glancer {
//-------------------------------------------------------------------//
PolyLine2D::PolyLine2D(const Point2D& from):
    fVectorSumm(0,0), fStartPoint(from){
}
//-------------------------------------------------------------------//
PolyLine2D::~PolyLine2D(){
    fVectors.clear();
}
//-------------------------------------------------------------------//
void PolyLine2D::AddVector(const Vector2D& vector){
    fVectorSumm += vector;
    fVectors.push_back(vector);
}
//-------------------------------------------------------------------//
const Point2D& PolyLine2D::GetStart() const{
    return fStartPoint;
}
//-------------------------------------------------------------------//
const Vector2D& PolyLine2D::GetVectorSumm() const{
    return fVectorSumm;
}
//-------------------------------------------------------------------//
const std::vector<Vector2D>& PolyLine2D::GetVectors() const{
    return fVectors;
}
//-------------------------------------------------------------------//
} // namespace glancer
