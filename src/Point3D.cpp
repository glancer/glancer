#include "Point3D.h"
#include "Vector3D.h"

namespace glancer {
//-------------------------------------------------------------------//
Point3D::Point3D(long _x, long _y, long _z) :
        x(_x), y(_y),z(_z) {
}
//-------------------------------------------------------------------//
Point3D::~Point3D(){
}
//-------------------------------------------------------------------//
long Point3D::X() const{
    return x;
}
//-------------------------------------------------------------------//
long Point3D::Y() const{
    return y;
}
//-------------------------------------------------------------------//
long Point3D::Z() const{
    return z;
}
//-------------------------------------------------------------------//
bool Point3D::operator ==(const Point3D& p) const{
    return x == p.x && y == p.y && z == p.z;
}
//-------------------------------------------------------------------//
Point3D& Point3D::operator = (const Point3D& p) {
    x = p.x; y = p.y; z = p.z;
    return *this;
}
//-------------------------------------------------------------------//
Point3D& Point3D::operator += (const Vector3D& v) {
    x += v.X(); y += v.Y(); z += v.Z();
    return *this;
}
//-------------------------------------------------------------------//
Point3D::Point3D(){
}
//-------------------------------------------------------------------//
const Point3D operator+(const Point3D&  p, const Vector3D& v){
    return Point3D(p.X()+v.X(), p.Y()+v.Y(), p.Z()+v.Z());
}
//-------------------------------------------------------------------//
const Point3D operator+(const Vector3D& v, const Point3D&  p){
    return Point3D(p.X()+v.X(), p.Y()+v.Y(), p.Z()+v.Z());
}
//-------------------------------------------------------------------//
} // namespace glancer
