#ifndef GLANCER_POLYLINE2D_H_
#define GLANCER_POLYLINE2D_H_

#include <vector>

#include "Point2D.h"
#include "Vector2D.h"

namespace glancer {
/**
 * Class for storing polyline.
 */
class PolyLine2D {
  public:
    PolyLine2D(const Point2D& from);
    ~PolyLine2D();
    /**
     * Adds next point as a vector
     */
    void AddVector(const Vector2D& vector);
    /**
     * Getters for access internal data
     */
    const Point2D& GetStart() const;
    const Vector2D& GetVectorSumm() const;
    const std::vector<Vector2D>& GetVectors() const;
  private:
    Point2D fStartPoint;
    std::vector <Vector2D> fVectors; 
    Vector2D fVectorSumm; // <-- We do keep a vector summ for an easy access
};
} // namespace glancer
#endif // GLANCER_POLYLINE2D_H_
