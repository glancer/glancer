#include <cmath> // for norm()

#include "Vector2D.h"

namespace glancer {
//-------------------------------------------------------------------//
Vector2D::Vector2D(long _x, long _y) :
        x(_x), y(_y) {
}
//-------------------------------------------------------------------//
Vector2D::Vector2D(const Point2D &from, const Point2D &to):
        x(to.x - from.x), y(to.y - from.y) {
}
//-------------------------------------------------------------------//
Vector2D::~Vector2D(){
}
//-------------------------------------------------------------------//
long Vector2D::X() const {
    return x;
}
//-------------------------------------------------------------------//
long Vector2D::Y() const {
    return y;
}
//-------------------------------------------------------------------//
long Vector2D::cross(const Vector2D &vec) const {
    return x*vec.y - y*vec.x;
}
//-------------------------------------------------------------------//
long Vector2D::dot(const Vector2D &p) const {
    return x*p.x + y*p.y;
}
//-------------------------------------------------------------------//
const long Vector2D::norm2() const {
    return x*x + y*y;
}
//-------------------------------------------------------------------//
const float Vector2D::norm() const {
    return std::sqrt(norm2());
}
//-------------------------------------------------------------------//
Vector2D& Vector2D::operator *= (const long s){
    x *= s; y *= s;
    return *this;
}
//-------------------------------------------------------------------//
Vector2D& Vector2D::operator *= (const float s){
    x *= s; y *= s;
    return *this;
}
//-------------------------------------------------------------------//
Vector2D& Vector2D::operator /= (const long s){
    x /= s; y /= s;
    return *this;
}
//-------------------------------------------------------------------//
Vector2D& Vector2D::operator += (const Vector2D& v){
    x += v.x; y+= v.y;
    return *this;
}
//-------------------------------------------------------------------//
Vector2D& Vector2D::operator -= (const Vector2D& v){
    x -= v.x; y -= v.y;
    return *this;
}
//-------------------------------------------------------------------//
} // namespace glancer
