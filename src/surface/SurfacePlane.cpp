#include "SurfacePlane.h"
#include "FVector3D.h"
#include "Vector3D.h"
#include "Matrix3D.h"

#include <iostream> // For debugging, will be removed

namespace glancer {
//-------------------------------------------------------------------//
SurfacePlane::SurfacePlane(Point3D &A, Point3D &B, Point3D &C):
        center_point (A) {
    
    // We want to switch to normalized float vectors as soon as possible
    // in order to prevent growing the norm of the vectors in integer space
    FVector3D x_axis(Vector3D(A, B)); 
    x_axis.normalize();
    FVector3D z_axis = x_axis.cross(FVector3D(Vector3D(A, C))); 
    z_axis.normalize();
    FVector3D y_axis = z_axis.cross(x_axis);
    // Debug, will remove
    /*
    std::cout << "Generated Plane with new basis " 
        << "X=[" << x_axis.X() << ";" << x_axis.Y() << ";" << x_axis.Z() << "] "
        << "Y=[" << y_axis.X() << ";" << y_axis.Y() << ";" << y_axis.Z() << "] "
        << "Z=[" << z_axis.X() << ";" << z_axis.Y() << ";" << z_axis.Z() << "] "
        << std::endl;
    */
    // Now we want to calculate quaternion
    // This is done by constructing basis rotation matrix first
    // Note, this matrix corresponds to Plane coordinates to world coordinates rotation
    Matrix3D rot_matrix;
    rot_matrix[0][0] = x_axis.X(); rot_matrix[0][1] = y_axis.X(); rot_matrix[0][2] = z_axis.X();
    rot_matrix[1][0] = x_axis.Y(); rot_matrix[1][1] = y_axis.Y(); rot_matrix[1][2] = z_axis.Y();
    rot_matrix[2][0] = x_axis.Z(); rot_matrix[2][1] = y_axis.Z(); rot_matrix[2][2] = z_axis.Z();
    quat.FromMatrix(rot_matrix);
    /*
    std::cout << "Resulting Quaternion "
        << "R=[" << quat.R().X() << ";" << quat.R().Y() << ";" << quat.R().Z() << "] "
        << "W="  << quat.W()
        << std::endl;
    */
}
//-------------------------------------------------------------------//
SurfacePlane::~SurfacePlane(){
}
//-------------------------------------------------------------------//
Point3D SurfacePlane::ToSurface(const Point3D &p) const{
    FVector3D vector(Vector3D(center_point, p)); 
    // Vector from center of the plane in world coordinates to point in the world coordinates
    // This takes care of translation, then, we'll need to rotate it by iverse quaternion.
    vector = quat.inverse() * vector;
    
    return Point3D(vector.X(), vector.Y(), vector.Z());
}
//-------------------------------------------------------------------//
Vector3D SurfacePlane::ToSurface(const Vector3D &v) const{
    FVector3D vector(v.X(),v.Y(),v.Z());
    vector = quat.inverse() * vector;
    return Vector3D(vector.X(), vector.Y(), vector.Z());
}
//-------------------------------------------------------------------//
float SurfacePlane::DistanceToSurface(const Point3D &p) const {
    FVector3D vector(Vector3D(center_point, p));
    vector = quat.inverse() * vector;
    return vector.Z();
}
//-------------------------------------------------------------------//
Point3D SurfacePlane::FromSurface(const Point2D &p) const{
    FVector3D vector(p.X(), p.Y(), 0);
    vector = quat * vector;
    return Point3D(center_point.X()+vector.X(), center_point.Y()+vector.Y(), center_point.Z()+vector.Z());
}
//-------------------------------------------------------------------//
Vector3D SurfacePlane::FromSurface(const Vector2D &v) const {
    FVector3D result(v.X(), v.Y(), 0);
    result = quat * result;
    return Vector3D(result.X(), result.Y(), result.Z());
}
//-------------------------------------------------------------------//
Vector3D SurfacePlane::FromSurface(const Vector3D &v){
    FVector3D result(v.X(), v.Y(), v.Z());
    result = quat * result;
    return Vector3D(result.X(), result.Y(), result.Z());
}
//-------------------------------------------------------------------//
const Point3D& SurfacePlane::GetCenterPoint() const{
    return center_point;
}
//-------------------------------------------------------------------//
} // namespace glancer
