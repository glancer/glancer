#ifndef GLANCER_SURFACEPLANE_H_
#define GLANCER_SURFACEPLANE_H_

#include "Point3D.h"
#include "Point2D.h"
#include "Vector2D.h"
#include "Quaternion.h"

namespace glancer {
/**
 * Definition for the plane slicing surface.
 * For each slice extruder moves perpendicular to the normal of the plane.
 * The condition of slicing surfaces collisions are not tested, so please make sure they do not
 * intersect within the volume of the mesh.
 */
class SurfacePlane {
  public:
    /**
     * The easiest definition of the plane is the plane passing through 3 points in world coordinates
     * system, then the new basis is constructed in the following way:
     * A is becoming zero point. 
     * Plane's X vector is oriented along AB
     * Plane's Z vector is oriented along AB x AC 
     * and then the Y axis is oriented along Z x X
     *
     * Then the rotation to the new basis is done via Quaternion and float vector math and translations
     * are done via integer vector math.
     * So all in all if you want reasonable precision, please choose these points correctly.
     * Also please notice that different surfaces should not intersect within your dataset.
     */
    SurfacePlane(Point3D &A, Point3D &B, Point3D &C);
    ~SurfacePlane();
    /**
     * Returns Point In surface Coordinates given a world coordinate point.
     */
    Point3D ToSurface(const Point3D &p) const;
    /**
     * Returns a vector in a surface coordinates, given a vector in world coordinate
     */
    Vector3D ToSurface(const Vector3D &v) const;
    /**
     * Returns Z position above or below the plane of a point given in a world coordinate
     */
    float DistanceToSurface(const Point3D &p) const;
    /**
     * Returns Point in a world coordinate given surface coordinates
     */
    Point3D FromSurface(const Point2D &p) const;
    /**
     * Returns a vector in a world coordinates, given a vector in surface coordinate
     */
    Vector3D FromSurface(const Vector2D &v) const;
    /**
     * Returns a vector in a world coordinate, given 3D vector in surface coordinate
     */
    Vector3D FromSurface(const Vector3D &v);

    /**
     * Returns center point.
     */
    const Point3D& GetCenterPoint() const;

  private:
    Point3D center_point;
    Quaternion  quat;
};
} // namespace glancer
#endif // GLANCER_SURFACEPLANE_H_
