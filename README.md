Glancer
=======
Glancer is non-planar slicer for multi-axis and multi-toolheads 3D printers. It generates g-code from 3D triangulated meshes.

License
===
Glancer is released under terms of the AGPLv3 License.
Terms of the license can be found in the LICENSE.md file or [here](http://www.gnu.org/licenses/agpl.html).

But in general it boils down to: You need to share the source of any Glancer modifications if you make an application with it. 
(Even if you make a web-based slicer, you still need to share the source!)
